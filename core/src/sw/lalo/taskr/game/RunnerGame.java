package sw.lalo.taskr.game;

import com.badlogic.gdx.Game;
import sw.lalo.taskr.game.screens.GameScreen;
import sw.lalo.taskr.game.screens.HowToPlayScreen;
import sw.lalo.taskr.game.screens.MainMenuScreen;
import sw.lalo.taskr.game.screens.RewardScreen;
import sw.lalo.taskr.game.screens.SplashScreen;


public class RunnerGame extends Game {
	
	public static final int HEIGHT = 600;
	public static final int WIDTH = 1000;
	public static final int PTM_RATIO = 100;
	public GameScreen gameScreen;
	public SplashScreen splashScreen;
	public MainMenuScreen menuScreen;
	public HowToPlayScreen HowToScreen;
	public RewardScreen rewardScreen;
	public static int [] m_GameMillestones = {100,200,400,800,1000,1300,1500,1800,2000,4000,5000,6000,8000,10000};
	public RunnerGame() {

	}
	
	@Override
	public void create () {
		splashScreen = new SplashScreen(this);
		gameScreen = new GameScreen(this);
		menuScreen =new MainMenuScreen(this);
		HowToScreen = new HowToPlayScreen(this);
		rewardScreen = new RewardScreen(this);
		setScreen(splashScreen);
	}


}
