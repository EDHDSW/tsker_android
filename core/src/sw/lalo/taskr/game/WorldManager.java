package sw.lalo.taskr.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2D;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import sw.lalo.taskr.game.gameobjects.Background;
import sw.lalo.taskr.game.gameobjects.StaticCharacter;
import sw.lalo.taskr.game.model.DynamicCharacterAnimation;
import sw.lalo.taskr.game.model.FlyingEnemy;
import sw.lalo.taskr.game.model.PlataformModel;
import sw.lalo.taskr.game.model.PlayerModel;
import sw.lalo.taskr.game.model.Star;
import sw.lalo.taskr.game.model.WorldModel;
import sw.lalo.taskr.game.model.barnacle;
import java.util.ArrayList;

public class WorldManager {
	public static boolean GAME_OVER = false;
	
	private World physicsWorld ;
	private Box2DDebugRenderer physicsRenderder;
 	private WorldModel m_worldMdel;

	private Background background ;
	private PlayerModel playerModel;

	private Music music;

	private Array<PlataformModel> statics;
	private Array<barnacle> enemies;
	private Array<FlyingEnemy> flyingEnemies;
	private Array<Star> m_Stars;
	
	private SpriteBatch batch;

	private int m_CountRandomSeed;
	private int m_RandomSeedBarnacle;
	private boolean m_boJumpSide;//0 =left,1 right ;
	Preferences m_Preferences;
	private Long m_Score;

	private OrthographicCamera cameraGUI;

	private Sprite m_ScoreStar;
	String [] m_Musicsources = new String[] {"audio/DanceAndJump.ogg",
			"audio/Epics8Bit.mp3",
			"audio/ShortSimple.mp3",
			"audio/JumpRun.ogg",
			"audio/ThroughThePortal.ogg",
			"audio/BlitzKaskade.mp3"};
	private int [] m_Gates =  new int[]{100,200,400,800,1000,1400,1600,2000,2500};

	int [] m_millestones = {100,200,400,800,1000,1300,1500,1800,2000,4000,5000,6000,8000,10000};
	                                 //1 2 3 4 5 6 7 8 9 0 1 2 3 4
	int [] index_milestonesPlataform ={0,1,1,2,2,3,3,4,4,5,5,5,5,5,};
	int [] index_milestonesPlayer =   {1,1,2,2,3,3,4,4,5,5,6,7,8,9,};
	final int noMillestones = 14;
	/**
	 * 
	 */
	public WorldManager(SpriteBatch batch) {



		GAME_OVER = false;
		m_boJumpSide = false;

		m_Preferences= Gdx.app.getPreferences("PreferencesScores");
		m_Score = m_Preferences.getLong("Score", 0);

		m_ScoreStar = new Sprite(new Texture("img/items/coinGold.png"));
		m_ScoreStar.setSize(90, 90);

		m_RandomSeedBarnacle = MathUtils.random(5)+2;
		m_CountRandomSeed = 0;

		physicsWorld = new World(new Vector2(0.0f, -10.0f), true);
		//physicsRenderder = new Box2DDebugRenderer();
		int millestone = getMillestone();
		int randomPlayer = 0;
		int radomPlataform = 0;
		if(millestone == noMillestones)
		{
			randomPlayer =0;
			radomPlataform =0;
		}
		else
		{
			randomPlayer = index_milestonesPlayer[millestone];
			radomPlataform = index_milestonesPlataform[millestone];

		}

		int Platformtype =  MathUtils.random(radomPlataform);
		int BackGroundtype = 0;
		int MusicType = 0;
		switch(Platformtype)
		{
			case 0: //grass
				BackGroundtype = MathUtils.random(2);
				MusicType=0;
				break;
			case 1: //dirt
				BackGroundtype = 3;
				MusicType =1;
				break;
			case 2://planet
				BackGroundtype = 4;
				MusicType =2;
				break;
			case 3://sand
				BackGroundtype = 5;
				MusicType =3;
				break;
			case 4://snow
				BackGroundtype = 6;
				MusicType =4;
				break;
			case 5://stone
				BackGroundtype = 1;
				MusicType =5;
				break;
			default://grass
				BackGroundtype = 2;
				MusicType =0;
				break;
		}
		background = new Background(BackGroundtype);
		//player = new Player(1, 2);//player = new Player(RunnerGame.WIDTH/2/RunnerGame.PTM_RATIO, 1.5f);
		background.createPhysics(physicsWorld);
		m_worldMdel = new WorldModel(Platformtype);
		int playerType =   MathUtils.random(randomPlayer);
		playerModel = new PlayerModel(4,8,playerType);
		playerModel.createPhysics(physicsWorld);

		statics = new Array<PlataformModel>();
		enemies = new Array<barnacle>();
		flyingEnemies = new Array<FlyingEnemy>();
		m_Stars = new Array<Star>();



			music = Gdx.audio.newMusic(Gdx.files.internal(m_Musicsources[MusicType]));
			music.setLooping(true);
			music.play();

		
		this.batch = batch;
		physicsWorld.setContactListener(new MyContactListener());
		PlataformModel modelInit = new PlataformModel(m_worldMdel.getPlataformInit());
		statics.add(modelInit);
		statics.get(statics.size - 1).createPhysics(physicsWorld);
		Box2D.init();
	}

	private int getMillestone()
	{
		int Reached = noMillestones;
		for (int cnt = 0;cnt< noMillestones;cnt++)
		{
			if(m_Score > m_millestones[cnt])
			{
				Reached  = cnt ;
			}

		}
		return Reached;
	}
 public void touchDown(float worldX, float worldY)
 {
    //if grounded :set palyer jumping, save side
	 //if left side m_boJumpSide =false; else {m_boJumpSide = true;}
	 boolean isGrounded = playerModel.isGrounded();
	 boolean isJumping = playerModel.isJumping();
	 int midside= RunnerGame.WIDTH/(2*RunnerGame.PTM_RATIO);
	 if(isGrounded)
	 {
		 playerModel.setJumping();
		 //save side

		 if(worldX < midside)
		 {
			 m_boJumpSide = false;
		 }
		 else
		 {
			 Gdx.app.log("touch right", "width"+midside);
			 m_boJumpSide = true;
		 }
	 }
	 //if grounded :set palyer jumping, save side
	 //if left side m_boJumpSide =false; else {m_boJumpSide = true;}
	 if(isJumping)
	 {
		 if((m_boJumpSide == false)&&(worldX> midside))
		 {
			 playerModel.setDoubleJumping();

		 }
		 if((m_boJumpSide == true)&&(worldX< midside))
		 {
			 playerModel.setDoubleJumping();
		 }
	 }
	 //if player jumping . set second jumping(if allowed). player set second jumping
	 //if left && m_boJumpSide == true, set jumping
	 //if right && m_boJumpSide == false set jumping
	 // else do nothing

 }
	public void touchUp(int screenX, int screenY)
	{
		playerModel.endJump();
	}

	public boolean draw() {
		boolean boPlayerAlive = true;




		if(statics.get(statics.size - 1).getX() < ((RunnerGame.WIDTH / RunnerGame.PTM_RATIO)+1) )
		{
			ArrayList<PlataformModel> plataforms = new ArrayList<PlataformModel>();
			plataforms = m_worldMdel.GetNextPlataForms();
			for (PlataformModel plataform : plataforms)
			{
				PlataformModel model = new PlataformModel(plataform);
                float gap = (1.0f* MathUtils.random(2))+1.0f;
				float x = statics.get(statics.size - 1).getX() + statics.get(statics.size - 1).getWidth()/2 + model.getWidth()/2+ gap;//;(model.getWidth()/2);//+(model.getWidth()/2);//; model.getX() ;//; statics.get(statics.size - 1).getWidth()/2)( +model.getWidth() ;
				model.SetSpritePositionX(x);
				statics.add(model);
				statics.get(statics.size - 1).createPhysics(physicsWorld);
				m_CountRandomSeed++;
				if ((m_CountRandomSeed >= m_RandomSeedBarnacle)&&(statics.get(statics.size - 1).getWidth()>2.0f))
				{


					float x_enemie = statics.get(statics.size - 1).getX()+ 0.5f;//+statics.get(statics.size - 1).getWidth()/2 -0.2f;
					float y_enemie = statics.get(statics.size - 1).getY()+statics.get(statics.size - 1).getHeight() + 0.5f ;

					float x_star = statics.get(statics.size - 1).getX()+ 0.5f;//+statics.get(statics.size - 1).getWidth()/2 -0.2f;
					float y_star= statics.get(statics.size - 1).getY()+statics.get(statics.size - 1).getHeight()  ;

					int switcher =  MathUtils.random(2);

					switch (switcher)
					{
						case 0:
							flyingEnemies.add(new FlyingEnemy(x_enemie, y_enemie ));
							flyingEnemies.get(flyingEnemies.size - 1).createPhysics(physicsWorld);
							break;
						case 1:
							enemies.add(new barnacle(x_enemie, y_enemie ));
							enemies.get(enemies.size - 1).createPhysics(physicsWorld);
							break;
						case 2:
							int numberstars= MathUtils.random(2)+1;
							int updown = MathUtils.random(2);
							if(updown == 0)
							{
								y_star = y_star+1.0f;
							}
							Sprite sprite = new Sprite(new Texture("img/items/coinGold.png"));
							for (int cnt = 0;cnt <numberstars;cnt ++)
							{
								Star star = new Star(sprite, x_star +(cnt*0.8f), y_star, 80, 80);
								m_Stars.add(star);
								m_Stars.get(m_Stars.size - 1).createPhysics(physicsWorld);
							}
							break;
						default:
							break;
					}
					m_RandomSeedBarnacle = MathUtils.random(3)+1;
					m_CountRandomSeed =0;

				}

				else if((m_CountRandomSeed >= m_RandomSeedBarnacle))
				{
					float x_enemie = statics.get(statics.size - 1).getX()+ 0.5f;//+statics.get(statics.size - 1).getWidth()/2 -0.2f;
					float y_enemie = statics.get(statics.size - 1).getY()+statics.get(statics.size - 1).getHeight() + 0.5f ;
					float x_star = statics.get(statics.size - 1).getX() + 0.5f;//+statics.get(statics.size - 1).getWidth()/2 -0.2f;
					float y_star = statics.get(statics.size - 1).getY() + statics.get(statics.size - 1).getHeight() ;
					int switcher =  MathUtils.random(2);

					if (switcher == 0)
					{

						flyingEnemies.add(new FlyingEnemy(x_enemie, y_enemie ));
						flyingEnemies.get(flyingEnemies.size - 1).createPhysics(physicsWorld);

					}
					else {

						int numberstars= MathUtils.random(2)+1;
						int updown = MathUtils.random(2);
						if(updown == 0)
						{
							y_star = y_star+1.0f;
						}
						Sprite sprite = new Sprite(new Texture("img/items/coinGold.png"));
						for (int cnt = 0;cnt <numberstars;cnt ++)
						{
							Star star = new Star(sprite, x_star +(cnt*0.8f), y_star, 80, 80);
							m_Stars.add(star);
							m_Stars.get(m_Stars.size - 1).createPhysics(physicsWorld);
						}
					}
					m_RandomSeedBarnacle = MathUtils.random(3)+1;
					m_CountRandomSeed =0;
				}

			}


		}
		for (int i = 0; i < statics.size; i++) {
			if (statics.get(i).getX() < -10.0f)
			{
				statics.get(i).destroyBody();
				statics.removeIndex(i);
			}
		}


		for (int i = 0; i < enemies.size; i++) {
			if ( (enemies.get(i).getX() < -10.0f) || (enemies.get(i).getY() < -10.0f) || (enemies.get(i).getY() > 20.0f))
			{
				enemies.get(i).destroyBody();
				enemies.removeIndex(i);
			}
		}
		for (int i = 0; i < flyingEnemies.size; i++) {
			if ( (flyingEnemies.get(i).getX() < -10.0f) || (flyingEnemies.get(i).getY() < -10.0f) || (flyingEnemies.get(i).getY() > 20.0f))
			{
				flyingEnemies.get(i).destroyBody();
				flyingEnemies.removeIndex(i);
			}
		}
		for (int i = 0; i < m_Stars.size; i++) {
			if ( (m_Stars.get(i).getX() < -10.0f) || (m_Stars.get(i).getY() < -10.0f) || (m_Stars.get(i).getY() > 20.0f)||(m_Stars.get(i).isTaken()))
			{
				m_Stars.get(i).destroyBody();
				m_Stars.removeIndex(i);
			}
		}

		if ( (playerModel.getY() < 0) || (playerModel.getX()< -5)) {
			GAME_OVER = true;
			playerModel.setDead(true);

		}



		batch.begin();
		background.draw(batch);

		for (StaticCharacter plataforma :statics)
		{
			plataforma.draw(batch);
		}

		for (DynamicCharacterAnimation enemy: enemies) {
			enemy.draw(batch);
			enemy.setResititution(0.8f);
		}
		for (FlyingEnemy enemy: flyingEnemies) {
			enemy.draw(batch);

		}
		for (Star star: m_Stars) {
			star.draw(batch);

		}
		playerModel.draw(batch);
		batch.draw(m_ScoreStar, 0f, 5.2f,0.9f,0.9f);
		batch.end();
		batch.begin();
		//physicsRenderder.render(physicsWorld, batch.getProjectionMatrix());
		physicsWorld.step(1 / 45f, 6, 2);

		batch.end();
		

		if(playerModel.isDead())
		{
			boPlayerAlive = false;
			music.stop();
			music.dispose();
			m_Preferences.putLong("Score", m_Score);
			m_Preferences.flush(); // This saves the preferences file.
		}
		return boPlayerAlive;
	}
	public void EndGame()
	{
		music.stop();
		music.dispose();
		m_Preferences.putLong("Score", m_Score);
		m_Preferences.flush(); // This saves the preferences file.
	}
	public long getScore()
	{
		return m_Score;
	}
	/**
	 * 
	 */
	public void dispose() {
		background.dispose();
		physicsWorld.dispose();
		physicsRenderder.dispose();
		playerModel.dispose();
		music.dispose();
		batch.dispose();
	}
	
	public Array<PlataformModel> getPlatforms() {
		return statics;
	}
	
	
	
	private class MyContactListener implements ContactListener {
		@Override
		public void beginContact(Contact contact) {
			
			final Fixture f1 = contact.getFixtureA();
			final Fixture f2 = contact.getFixtureB();
			if ( (playerModel.getY() < 0) || (playerModel.getX()< -5)) {
				GAME_OVER = true;
				playerModel.setDead(true);
			}
			for (FlyingEnemy enemy: flyingEnemies) {
				if (f1.equals(playerModel.getFixture()) && f2.equals(enemy.getFixture())) {

					GAME_OVER = true;
					playerModel.setDead(true);
				}

				if (f2.equals(playerModel.getFixture()) && f1.equals(enemy.getFixture())) {
					GAME_OVER = true;
					playerModel.setDead(true);

				}
			}
			for (Star star: m_Stars) {
				if (f1.equals(playerModel.getFixture()) && f2.equals(star.getFixture())) {

					star.setTaken();
					m_Score++;
					m_Preferences.putLong("Score", m_Score);
					m_Preferences.flush(); // This saves the preferences file.

				}

				if (f2.equals(playerModel.getFixture()) && f1.equals(star.getFixture())) {

					star.setTaken();
					m_Score++;
					m_Preferences.putLong("Score", m_Score);
					m_Preferences.flush(); // This saves the preferences file.
				}
			}

			for (DynamicCharacterAnimation enemy: enemies) {
				if (f1.equals(playerModel.getFixture()) && f2.equals(enemy.getFixture())) {
					GAME_OVER = true;
					playerModel.setDead(true);
				} 
				
				if (f2.equals(playerModel.getFixture()) && f1.equals(enemy.getFixture())) {
					GAME_OVER = true;
					playerModel.setDead(true);
				} 
			}
		}

		@Override
		public void endContact(Contact contact) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void preSolve(Contact contact, Manifold oldManifold) {

		}

		@Override
		public void postSolve(Contact contact, ContactImpulse impulse) {
			// TODO Auto-generated method stub
			
		}
	}
}
