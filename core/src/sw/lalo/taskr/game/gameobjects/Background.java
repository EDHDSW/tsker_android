package sw.lalo.taskr.game.gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.World;
import sw.lalo.taskr.game.RunnerGame;
import sw.lalo.taskr.game.WorldManager;

public class Background implements Drawable {
	

	private Sprite back1;
	private Sprite back2;
	private float moveSpeed = 0.07f;
	private String[] m_sBackGrounds;

	public Background(int type) {

		m_sBackGrounds = new String[] {"img/backgrounds/blue_grass.png",
				"img/backgrounds/blue_shroom.png",
				"img/backgrounds/colored_desert.png",
				"img/backgrounds/blue_grass.png",
				"img/backgrounds/colored_land.png",
				"img/backgrounds/blue_desert.png",
				"img/backgrounds/blue_land.png",
				"img/backgrounds/colored_shroom.png"};

        back1 = new Sprite(new Texture(m_sBackGrounds[type]));
		back1.setSize(RunnerGame.WIDTH,RunnerGame.HEIGHT);
		back2 = new Sprite(back1);
		back2.setSize(RunnerGame.WIDTH, RunnerGame.HEIGHT);
		back1.setX(0);
		back2.setX(back1.getWidth() / RunnerGame.PTM_RATIO);
		back1.setY(RunnerGame.HEIGHT / RunnerGame.PTM_RATIO - back1.getHeight() / RunnerGame.PTM_RATIO);
		back2.setY(RunnerGame.HEIGHT / RunnerGame.PTM_RATIO - back2.getHeight() / RunnerGame.PTM_RATIO);
	}
	
	@Override
	public void createPhysics(World physicsWorld) {
	}
	
	@Override
	public void draw(SpriteBatch batch) {
		batch.draw(back1, back1.getX(), back1.getY(), back1.getWidth() / RunnerGame.PTM_RATIO, back1.getHeight() / RunnerGame.PTM_RATIO);
		batch.draw(back2, back2.getX(), back2.getY(), back2.getWidth() / RunnerGame.PTM_RATIO, back2.getHeight() / RunnerGame.PTM_RATIO);

		if (!WorldManager.GAME_OVER) {
			back1.translateX(-moveSpeed );
			back2.translateX(-moveSpeed);

		}
		
		//If the second background image is filling the screen, loop back to the first
		if (back2.getX() < 0) {
			back1.setX(0);
			back2.setX((back1.getX() + back1.getWidth()) / RunnerGame.PTM_RATIO);	
		}
	}
	
	@Override
	public void dispose() {
		back1.getTexture().dispose();
		back2.getTexture().dispose();
	}

	/**
	 * 
	 * @return
	 */

	@Override
	public float getX() {
		return back2.getX() / RunnerGame.PTM_RATIO;
	}

	@Override
	public float getY() {
		return back1.getY() / RunnerGame.PTM_RATIO;
	}

	@Override
	public float getWidth() {
		return back1.getWidth() / RunnerGame.PTM_RATIO + back2.getWidth() / RunnerGame.PTM_RATIO;
	}
	@Override
	public float getHeight() {
		return 1.0f;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
