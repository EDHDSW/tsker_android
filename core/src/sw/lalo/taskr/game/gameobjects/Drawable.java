package sw.lalo.taskr.game.gameobjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.World;

public interface Drawable {

	public void draw(SpriteBatch batch);
	public void dispose();
	public void createPhysics(World physicsWorld);
	public float getX();
	public float getY();
	public float getWidth();
	public float getHeight();
}
