package sw.lalo.taskr.game.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AnimationModel {
	private int m_rows;
	private int m_cols;

	private Animation anims;
	private Texture animSheets;
	private TextureRegion[] frames;

	public AnimationModel(String sheet, int rows, int cols, float stateTime,float width,float height) {
		this.m_rows = rows;
		this.m_cols = cols;
		animSheets = new Texture(Gdx.files.internal(sheet));

		TextureRegion[][] tmp = TextureRegion.split(animSheets, animSheets.getWidth() / cols, animSheets.getHeight() / rows);
		frames= new TextureRegion[cols * rows];
		int index = 0;
		for (int j = 0; j < rows; j++) {
			for (int k = 0; k < cols; k++)
			{
				frames[index++] = tmp[j][k];
				}
			}
			anims = new Animation(stateTime, frames);
	}

	public Animation getAnimation() {
		return anims;
	}

	public float getWidth() {
		return animSheets.getWidth() / m_cols;
	}

	public float getHeight() {
		return animSheets.getHeight() / m_rows;
	}
}
