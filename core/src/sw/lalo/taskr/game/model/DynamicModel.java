package sw.lalo.taskr.game.model;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import sw.lalo.taskr.game.RunnerGame;
import sw.lalo.taskr.game.gameobjects.Drawable;

//dynakic model used  without animation
public class DynamicModel implements Drawable {
	private Body dynamicBody;
	private float stateTime = 0f;
	private float x, y;
	private Sprite m_Sprite;
	private boolean dead;

	public DynamicModel(float x, float y){
		this.x = x;
		this.y = y;
	}

	@Override
	public void draw(SpriteBatch batch) {
	}

	@Override
	public void dispose() {
		m_Sprite.getTexture().dispose();
		dynamicBody.destroyFixture(getFixture());
		dynamicBody.getWorld().destroyBody(dynamicBody);
	}

	@Override
	public void createPhysics(World physicsWorld) {
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set(x - getWidth()/2, y - getHeight()/2);
		//bodyDef.position.set(m_Sprite.getX(), m_Sprite.getY());
		
		setDynamicBody(physicsWorld.createBody(bodyDef));

		PolygonShape playerBox = new PolygonShape();  
		playerBox.setAsBox(getWidth()/2, getHeight()/2);
		
		getDynamicBody().createFixture(playerBox, 1200.0f); 
		playerBox.dispose();
		
		getDynamicBody().setUserData(m_Sprite);
		getDynamicBody().setFixedRotation(true);
	}

	@Override
	public float getX() {
		return (getDynamicBody().getPosition().x - (getWidth()/2));
	}
	
	@Override
	public float getY() {
		return (getDynamicBody().getPosition().y-(getHeight()/2));
	}
	
	@Override
	public float getWidth() {
		return (m_Sprite).getWidth() / RunnerGame.PTM_RATIO;
	}
	
	@Override
	public float getHeight() {
		return (m_Sprite).getHeight()  / RunnerGame.PTM_RATIO;
	}


	public Body getDynamicBody() {
		return dynamicBody;
	}

	public void setDynamicBody(Body dynamicBody) {
		this.dynamicBody = dynamicBody;
	}

	public Sprite getSprite() {
		return m_Sprite;
	}


	public void setSprite(Sprite sprite) {
		this.m_Sprite = new Sprite(sprite);
	}

	public float getStateTime() {
		return stateTime;
	}


	public void setStateTime(float stateTime) {
		this.stateTime = stateTime;
	}


	public Fixture getFixture() {
		return dynamicBody.getFixtureList().get(0);
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}

	public boolean isDead() {
		return dead;
	}

	public void setResititution(float restitution) {
		getDynamicBody().getFixtureList().get(0).setRestitution(restitution);
	}

}
