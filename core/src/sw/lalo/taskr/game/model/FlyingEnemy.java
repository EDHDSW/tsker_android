package sw.lalo.taskr.game.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import sw.lalo.taskr.game.WorldManager;
import sw.lalo.taskr.game.gameobjects.StaticCharacter;

/**
 * Created by Eduardo on 12/04/2016.
 */
public class FlyingEnemy extends StaticCharacterAnimation {

    public FlyingEnemy(float x, float y) {

        super(x, y);
        setAnimations(new AnimationModel("img/Enemies/fly_sheet.png", 1, 2, 0.5f, 100.0f, 100.0f));

    }

    @Override
    public void draw(SpriteBatch batch) {
        setStateTime(getStateTime() + Gdx.graphics.getDeltaTime());

        if (!isDead()) {
            setCurrentFrame((TextureRegion)getAnimations().getAnimation().getKeyFrame(getStateTime(),true));
            batch.draw(getCurrentFrame(), getX(), getY(), getWidth(),getHeight());
            //getDynamicBody().setLinearVelocity(-2.8f, 0.0f);
            getBody().setLinearVelocity(-3.2f, 0.0f);
        }
        if (WorldManager.GAME_OVER) {
            getBody().setActive(false);
        }
    }

    @Override
    public void dispose() {
        super.dispose();

    }
    public void destroyBody()
    {
        getBody().destroyFixture(getFixture());
        getBody().getWorld().destroyBody(getBody());
    }
}
