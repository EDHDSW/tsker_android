package sw.lalo.taskr.game.model;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import sw.lalo.taskr.game.WorldManager;
import sw.lalo.taskr.game.gameobjects.StaticCharacter;

/**
 * Created by Eduardo on 12/04/2016.
 */
public class PlataformModel extends StaticCharacter {
    //1 meter = 100

    public PlataformModel(Sprite sprite, float x, float y,float width,float height) {

        super(sprite);
        getSprite().setSize(width, height);
        getSprite().setX(x);
        getSprite().setY(y);


    }

    public PlataformModel(PlataformModel plataform) {

        super(plataform.getSprite());
    }
    public void destroyBody()
    {
        getKinematicBody().destroyFixture(getFixture());
        getKinematicBody().getWorld().destroyBody(getKinematicBody());
    }

     public void SetSpritePositionX(float x)
     {
         getSprite().setX(x);
     }
    public void SetSpritePositionY(float y)
    {
        getSprite().setY(y);
    }

    @Override
    public void draw(SpriteBatch batch) {
       batch.draw(getSprite(), getKinematicBody().getPosition().x - getWidth()/2, getKinematicBody().getPosition().y - getHeight()/2, getWidth(), getHeight());
        if (!WorldManager.GAME_OVER) {
            getSprite().setPosition(getKinematicBody().getPosition().x, getKinematicBody().getPosition().y);
            getKinematicBody().setLinearVelocity(-2.0f, 0f);
        } else {
            getKinematicBody().setActive(false);
        }
    }
}
