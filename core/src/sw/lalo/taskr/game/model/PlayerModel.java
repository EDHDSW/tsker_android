package sw.lalo.taskr.game.model;

/**
 * Created by Eduardo on 29/04/2016.
 */
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import com.badlogic.gdx.math.Vector2;

public class PlayerModel extends DynamicModel {
    private static final float MAX_JUMP_HEIGHT = 5f;
    private boolean jumping ;
    private boolean DoubleJumping ;
    private boolean grounded;
    private boolean jumpForce;
    float rot;
    private int hysteresys;
    String [] m_sAnimals = new String[] {"img/animals/parrot.png",
            "img/animals/elephant.png",
            "img/animals/giraffe.png",
            "img/animals/hippo.png",
            "img/animals/monkey.png",
            "img/animals/panda.png",
            "img/animals/penguin.png",
            "img/animals/pig.png",
            "img/animals/rabbit.png",
            "img/animals/snake.png"};

    public PlayerModel(float x, float y,int type) {
        super(x, y);
        float width =80;
        float height = 80;
        rot =0;
        jumping = false;
        grounded = true;
        DoubleJumping = false;
        jumpForce = false;
        hysteresys =  0;
        this.setSprite( new Sprite(new Texture(m_sAnimals[type])));
        getSprite().setX(x);
        getSprite().setY(y);
        switch (type)
        {
            case 1:
            case 4:
            case 5:
            case 7:
                width = 80;
                height = 70;
                break;
            case 2:
            case 8:
            case 9:
                width = 70;
                height = 80;
                break;
            default:
                width =80;
                height =80;
                break;
        }
        getSprite().setSize(width,height );
    }

    @Override
    public void draw(SpriteBatch batch) {

        setStateTime(getStateTime() + Gdx.graphics.getDeltaTime());
        Vector2 linearV = getDynamicBody().getLinearVelocity();
        float velocity = linearV.len();
        if (jumpForce) {
            if (getDynamicBody().getPosition().y < MAX_JUMP_HEIGHT) {
                if (velocity < 5.5) {
                    getDynamicBody().applyLinearImpulse(0, 400, getDynamicBody().getPosition().x, getDynamicBody().getPosition().y, true);
                    hysteresys++;
                } else {
                    jumpForce = false;
                }


            } else {

                   jumpForce = false;
            }
        }
        if ((velocity == 0.0f) && (hysteresys > 3)) {
            jumping = false;
            grounded = true;
            DoubleJumping = false;
            hysteresys = 0;
        }

        rot = (rot - Gdx.graphics.getDeltaTime() *
                190) % 360;

        getSprite().setRotation(rot);

        batch.draw(getSprite(), getDynamicBody().getPosition().x - (getWidth() / 2), getDynamicBody().getPosition().y - (getHeight() / 2),
                getWidth()/2,getHeight()/2, getWidth(), getHeight(), 1, 1, rot);
    }


    @Override
    public void dispose() {
        super.dispose();
    }


    public boolean isJumping() {
        return jumping;
    }
    public boolean isDoubleJumping() {
        return DoubleJumping;
    }



    public void setJumping() {
        if (!jumping) {
            jumpForce = true;
            jumping = true;
            grounded = false;
            DoubleJumping = false;
        }
    }
    public void setDoubleJumping() {
        if (jumping) {
            getDynamicBody().setLinearVelocity(0,0);
            getDynamicBody().applyLinearImpulse(0, 400, getDynamicBody().getPosition().x, getDynamicBody().getPosition().y, true);
            jumpForce = true;
            DoubleJumping = true;
            grounded = false;
            jumping = false;
        }
    }
    public void setVelocity()
    {
        getDynamicBody().applyLinearImpulse(200, 0, getDynamicBody().getPosition().x, getDynamicBody().getPosition().y, true);
    }
    public void endJump()
    {
        jumpForce = false;
    }


    public boolean isGrounded() {
        return grounded;
    }


    public void setGrounded(boolean grounded) {

        this.grounded = grounded;
    }


}

