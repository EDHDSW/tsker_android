package sw.lalo.taskr.game.model;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import sw.lalo.taskr.game.WorldManager;

/**
 * Created by Eduardo on 12/04/2016.
 */

public class Star extends StaticCharacterStra {


    //1 meter = 100

    boolean m_Taken;
    public Star(Sprite sprite, float x, float y, float width, float height) {

        super(sprite);
        getSprite().setSize(width, height);
        getSprite().setX(x);
        getSprite().setY(y);
        m_Taken = false;


    }

    public Star(Star plataform) {

        super(plataform.getSprite());
    }

     public void SetSpritePositionX(float x)
     {
         getSprite().setX(x);
     }
    public void SetSpritePositionY(float y)
    {
        getSprite().setY(y);
    }

    public void destroyBody()
    {
        getKinematicBody().destroyFixture(getFixture());
        getKinematicBody().getWorld().destroyBody(getKinematicBody());
    }
    @Override
    public void draw(SpriteBatch batch) {
       batch.draw(getSprite(), getKinematicBody().getPosition().x - getWidth()/2, getKinematicBody().getPosition().y - getHeight()/2, getWidth(), getHeight());
        if (!WorldManager.GAME_OVER) {
            getSprite().setPosition(getKinematicBody().getPosition().x, getKinematicBody().getPosition().y);
            getKinematicBody().setLinearVelocity(-2.0f, 0f);
        } else {
            getKinematicBody().setActive(false);
        }
    }
    public void setTaken()
    {
        m_Taken = true;
    }
    public boolean isTaken()
    {
        return m_Taken;
    }

}
