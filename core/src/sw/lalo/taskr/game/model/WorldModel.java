
/**
 * Created by Eduardo on 11/04/2016.
 */
package sw.lalo.taskr.game.model;

import com.badlogic.gdx.graphics.g2d.Sprite;
import java.util.ArrayList;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Eduardo on 11/04/2016.
 */
public class WorldModel {
    //All ready created sprites and Objects an loaded in th background thread
    //public static final int HEIGHT = 600 = 6 meters					//Global game height
   // public static final int WIDTH = 1000 =  10 meters
    public static int PLATFORMS_NUMBERS = 9;
    private static String m_nsPlatform = "img/platform.png";
    private static String m_nsPlatformLeft = "img/platform.png";

    String [] m_sOneSegmentsW = new String[] {"img/Ground/Grass/grass.png",
            "img/Ground/Dirt/dirt.png",
            "img/Ground/Planet/planet.png",
            "img/Ground/Sand/sand.png",
            "img/Ground/Snow/snow.png",
            "img/Ground/Stone/stone.png"};

    String [] m_sTwoSegmentsW = new String[] {"img/Ground/Grass/grassTwo.png",
            "img/Ground/Dirt/dirtTwo.png",
            "img/Ground/Planet/planetTwo.png",
            "img/Ground/Sand/sandTwo.png",
            "img/Ground/Snow/snowTwo.png",
            "img/Ground/Stone/stoneTwo.png"};

    String [] m_sThreeSegmentsW = new String[] {"img/Ground/Grass/grassThree.png",
            "img/Ground/Dirt/dirtThree.png",
            "img/Ground/Planet/planetThree.png",
            "img/Ground/Sand/sandThree.png",
            "img/Ground/Snow/snowThree.png",
            "img/Ground/Stone/stoneThree.png"};

    String [] m_sFourSegmentsW = new String[] {"img/Ground/Grass/grassFour.png",
            "img/Ground/Dirt/dirtFour.png",
            "img/Ground/Planet/planetFour.png",
            "img/Ground/Sand/sandFour.png",
            "img/Ground/Snow/snowFour.png",
            "img/Ground/Stone/stoneFour.png"};

    String [] m_sFiveSegmentsW = new String[] {"img/Ground/Grass/grassFive.png",
            "img/Ground/Dirt/dirtFive.png",
            "img/Ground/Planet/planetFive.png",
            "img/Ground/Sand/sandFive.png",
            "img/Ground/Snow/snowFive.png",
            "img/Ground/Stone/stoneFive.png"};

    Sprite m_OneSprite;
    Sprite m_TwoSprite;
    Sprite m_ThreeSprite;
    Sprite m_FourSprite;
    Sprite m_FiveSprite;
    Sprite m_OneSpriteH;
    Sprite m_TwoSpriteH;
    Sprite m_ThreeSpriteH;
    Sprite m_FourSpriteH;
    Sprite m_FiveSpriteH;
    public static float m_nHeight = 0.5f;
    public static float m_nHeight2 = 2.2f;

    public static float segmentHeight = 100.0f;
    public static float segmentHeightHalf = 50.0f;
    public static float segmentHeightHalfE = 80.0f;

    PlataformModel PlatformInit;
    ArrayList<PlataformModel> Platforms0;
    ArrayList<PlataformModel> Platforms1;
    ArrayList<PlataformModel> Platforms2;
    ArrayList<PlataformModel> Platforms3;
    ArrayList<PlataformModel> Platforms4;
    ArrayList<PlataformModel> Platforms5;
    ArrayList<PlataformModel> Platforms6;
    ArrayList<PlataformModel> Platforms7;
    ArrayList<PlataformModel> Platforms8;
    ArrayList<PlataformModel> Platforms9;

    Map<Integer,ArrayList<PlataformModel>> m_PlataformWorlds;

    int Worlds_Index[];
    private int m_type ;
    public WorldModel(int type)
    {
        m_type = type;


        m_OneSprite = new Sprite(new Texture(m_sOneSegmentsW[m_type]));
        m_TwoSprite = new Sprite(new Texture(m_sTwoSegmentsW[m_type]));
        m_ThreeSprite = new Sprite(new Texture(m_sThreeSegmentsW[m_type]));
        m_FourSprite = new Sprite(new Texture(m_sFourSegmentsW[m_type]));
        m_FiveSprite = new Sprite(new Texture(m_sFiveSegmentsW[m_type]));

        m_OneSpriteH = new Sprite(new Texture(m_sOneSegmentsW[m_type]));
        m_TwoSpriteH = new Sprite(new Texture(m_sOneSegmentsW[m_type]));
        m_ThreeSpriteH = new Sprite(new Texture(m_sOneSegmentsW[m_type]));
        m_FourSpriteH = new Sprite(new Texture(m_sOneSegmentsW[m_type]));
        m_FiveSpriteH = new Sprite(new Texture(m_sOneSegmentsW[m_type]));



        m_PlataformWorlds = new HashMap<Integer,ArrayList<PlataformModel>>();
        createInitPlataform();
        createPlatform0();
        createPlatform1();
        createPlatform2();
        createPlatform3();
        createPlatform4();
        createPlatform5();
        createPlatform6();
        createPlatform7();
        createPlatform8();
        createPlatform9();

        /*the 3 stages that are gona be cycling */
        Worlds_Index = new int[3];
        Worlds_Index[0] = MathUtils.random(PLATFORMS_NUMBERS);
        Worlds_Index[1] = MathUtils.random(PLATFORMS_NUMBERS);
        Worlds_Index[2] = MathUtils.random(PLATFORMS_NUMBERS);

        //Create the plataforms

    }

    public ArrayList<PlataformModel>  GetNextPlataForms()
    {
        ArrayList<PlataformModel> auxiliar =  m_PlataformWorlds.get(Worlds_Index[0]);
        //return Position zero and update the index
        Worlds_Index[0] =  Worlds_Index[1];
        Worlds_Index[1] =  Worlds_Index[2];
        Worlds_Index[2] =  MathUtils.random(PLATFORMS_NUMBERS);
        return auxiliar;
    }
    //each stage  width has to sum 10 is the wwrld
    //pyshic representationis 10x6
    public PlataformModel getPlataformInit()
    {
        return PlatformInit;
    }
    private void createInitPlataform()
    {
    //----------
        Sprite sprite = new Sprite(new Texture(m_sFiveSegmentsW[m_type]));
        PlatformInit = new PlataformModel(sprite,5.0f,m_nHeight,1500.0f,50.f);

    }

    private void createPlatform0()
    {

        //----_----
        Platforms0 = new ArrayList<PlataformModel>();
        Platforms0.add(new PlataformModel(m_FourSprite,0.0f,m_nHeight,400.0f,segmentHeight));
        Platforms0.add(new PlataformModel(m_FiveSprite,1.0f,m_nHeight,500.0f,segmentHeight));
        m_PlataformWorlds.put(0, Platforms0);

    }


    private void createPlatform1()
    {

        //---_---__-
        Platforms1 = new ArrayList<PlataformModel>();
        Platforms1.add(new PlataformModel(m_ThreeSprite, 0.0f, m_nHeight, 300.0f, segmentHeight));
        Platforms1.add(new PlataformModel(m_ThreeSprite, 4.0f, m_nHeight, 300.0f, segmentHeight));
        Platforms1.add(new PlataformModel(m_OneSprite, 9.0f, m_nHeight, 100.0f, segmentHeight));
        m_PlataformWorlds.put(1, Platforms1);
    }

    private void createPlatform2()
    {

        //-_---__---
        Platforms2 = new ArrayList<PlataformModel>();
        Platforms2.add(new PlataformModel(m_OneSprite,0.0f,m_nHeight,100.0f,segmentHeight));
        Platforms2.add(new PlataformModel(m_ThreeSprite,2.0f,m_nHeight,300.0f,segmentHeight));
        Platforms2.add(new PlataformModel(m_ThreeSprite,7.0f,m_nHeight,300.0f,segmentHeight));
        m_PlataformWorlds.put(2, Platforms2);
    }

    private void createPlatform3()
    {
        //--_-_--__-
        Platforms3 = new ArrayList<PlataformModel>();
        Platforms3.add(new PlataformModel(m_TwoSprite, 0.0f, m_nHeight, 200.0f, segmentHeight));
        Platforms3.add(new PlataformModel(m_OneSprite, 3.0f, m_nHeight, 100.0f, segmentHeight));
        Platforms3.add(new PlataformModel(m_TwoSprite, 5.0f, m_nHeight, 200.0f, segmentHeight));
        Platforms3.add(new PlataformModel(m_OneSprite, 9.0f, m_nHeight, 100.0f, segmentHeight));
        m_PlataformWorlds.put(3, Platforms3);
    }

    private void createPlatform4()
    {

        //  ---- ---
        //-_    _
        Platforms4= new ArrayList<PlataformModel>();
        Platforms4.add(new PlataformModel(m_OneSprite, 0.0f, m_nHeight,100.0f, segmentHeight));
        Platforms4.add(new PlataformModel(m_FourSprite, 2.0f, m_nHeight2, 400.0f, segmentHeightHalfE));
        Platforms4.add(new PlataformModel(m_ThreeSprite, 7.0f, m_nHeight, 300.0f, segmentHeight));
        m_PlataformWorlds.put(4, Platforms4);
    }

    private void createPlatform5()
    {
        //  ---- ---
        //-_    _
        Platforms5 = new ArrayList<PlataformModel>();
        Platforms5= new ArrayList<PlataformModel>();
        Platforms5.add(new PlataformModel(m_OneSprite, 0.0f, m_nHeight2,100.0f, segmentHeightHalf));
        Platforms5.add(new PlataformModel(m_FourSprite, 2.0f, m_nHeight2, 400.0f, segmentHeightHalfE));
        Platforms5.add(new PlataformModel(m_ThreeSprite, 7.0f, m_nHeight2, 300.0f, segmentHeightHalf));
        m_PlataformWorlds.put(5, Platforms5);

    }

    private void createPlatform6()
    {

        //- - - - - -
        // _
        Platforms6 = new ArrayList<PlataformModel>();
        Platforms6.add(new PlataformModel(m_OneSprite, 0.0f, m_nHeight2, 100.0f, segmentHeightHalf));
        Platforms6.add(new PlataformModel(m_OneSprite, 2.0f, m_nHeight2, 100.0f, segmentHeightHalfE));
        Platforms6.add(new PlataformModel(m_OneSprite, 4.0f, m_nHeight2, 100.0f, segmentHeightHalfE));
        Platforms6.add(new PlataformModel(m_OneSprite, 6.0f, m_nHeight2, 100.0f, segmentHeightHalf));
        Platforms6.add(new PlataformModel(m_OneSprite, 8.0f, m_nHeight2, 100.0f, segmentHeightHalfE));
        m_PlataformWorlds.put(6, Platforms6);

    }

    private void createPlatform7()

    {
        Platforms7 = new ArrayList<PlataformModel>();

        Platforms7.add(new PlataformModel(m_FiveSprite, 0.0f, m_nHeight2, 500.0f, segmentHeightHalf));
        Platforms7.add(new PlataformModel(m_OneSprite, 6.0f, m_nHeight2, 100.0f, segmentHeightHalf));
        Platforms7.add(new PlataformModel(m_TwoSprite, 8.0f, m_nHeight, 200.0f, segmentHeight));

        m_PlataformWorlds.put(7, Platforms7);
    }

    private void createPlatform8()
    {
        Platforms8 = new ArrayList<PlataformModel>();
        Platforms8.add(new PlataformModel(m_TwoSprite, 0.0f, m_nHeight, 200.0f, segmentHeight));
        Platforms8.add(new PlataformModel(m_TwoSprite, 3.0f, m_nHeight, 200.0f, segmentHeight));
        Platforms8.add(new PlataformModel(m_TwoSprite, 6.0f, m_nHeight2, 200.0f, segmentHeightHalfE));
        Platforms8.add(new PlataformModel(m_OneSprite, 9.0f, m_nHeight2, 100.0f, segmentHeightHalf));
        m_PlataformWorlds.put(8, Platforms8);
    }

    private void createPlatform9()
    {
        Platforms9 = new ArrayList<PlataformModel>();

        Platforms9.add(new PlataformModel(m_TwoSprite, 0.0f, m_nHeight, 200.0f, segmentHeight));
        Platforms9.add(new PlataformModel(m_OneSprite, 3.0f, m_nHeight2, 100.0f, segmentHeightHalfE));
        Platforms9.add(new PlataformModel(m_TwoSprite, 5.0f, m_nHeight2, 200.0f, segmentHeightHalf));
        Platforms9.add(new PlataformModel(m_OneSprite, 8.0f, m_nHeight2, 100.0f, segmentHeightHalf));
        m_PlataformWorlds.put(9, Platforms9);
    }


}
