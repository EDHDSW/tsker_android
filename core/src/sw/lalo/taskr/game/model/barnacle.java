package sw.lalo.taskr.game.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import sw.lalo.taskr.game.WorldManager;

public class barnacle extends DynamicCharacterAnimation {

	private TextureRegion deathImage ;

	public barnacle(float x, float y) {
		super(x, y);
		deathImage = new TextureRegion(new Texture("img/Enemies/barnacle_dead.png"));
		setAnimations(new AnimationModel("img/Enemies/barnacle_sheet.png",1, 2, 0.5f,100.0f,100.0f));
	}

	@Override
	public void draw(SpriteBatch batch) {
		setStateTime(getStateTime() + Gdx.graphics.getDeltaTime());
		
		if (!isDead()) {
			setCurrentFrame((TextureRegion) getAnimations().getAnimation().getKeyFrame(getStateTime(),true));
			batch.draw(getCurrentFrame(), getX(), getY(), getWidth(),getHeight());
			getDynamicBody().setLinearVelocity(-2.0f,0.0f);
		} else {
			setCurrentFrame(deathImage);
		}
		if (WorldManager.GAME_OVER) {
			getDynamicBody().setActive(false);
		}
	}
	
	@Override
	public void dispose() {
		super.dispose();
		deathImage.getTexture().dispose();
	}
	public void destroyBody()
	{
		getDynamicBody().destroyFixture(getFixture());
		getDynamicBody().getWorld().destroyBody(getDynamicBody());
	}
}
