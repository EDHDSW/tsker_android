
package sw.lalo.taskr.game.screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import sw.lalo.taskr.game.RunnerGame;
import sw.lalo.taskr.game.WorldManager;
import com.badlogic.gdx.InputProcessor;

public class GameScreen implements Screen,InputProcessor {


	private BitmapFont m_DefaultFont;
	private Label m_lableScore;
	private Stage stage ;
	private WorldManager worldManager;
	private RunnerGame m_Game;
	private Skin buttonCloseskin;
	private TextButton buttonClose;

	Label.LabelStyle m_LabelStyle;
	Camera score_Cam;
	SpriteBatch score_batch;
	private long m_lIntinialScore;
	public GameScreen(RunnerGame game)
	{

		m_Game = game;

	}
	@Override
	public void show() {

		Gdx.input.setInputProcessor(this);
		stage = new Stage(new StretchViewport(RunnerGame.WIDTH / RunnerGame.PTM_RATIO, RunnerGame.HEIGHT / RunnerGame.PTM_RATIO));
		worldManager = new WorldManager((SpriteBatch) stage.getBatch());

		score_batch = new SpriteBatch();
		score_Cam = new OrthographicCamera(RunnerGame.WIDTH,
				RunnerGame.HEIGHT);
		score_Cam.position.set(0, 0, 0);
		score_Cam.update();
		m_DefaultFont = new BitmapFont(Gdx.files.internal("fonts/gothic50.fnt"), Gdx.files.internal("fonts/gothic50.png"), false);
		m_DefaultFont.setColor(0, 0, 0, 1);
		m_LabelStyle = new Label.LabelStyle();
		m_LabelStyle.font = m_DefaultFont;
		m_lableScore = new Label("lalo",m_LabelStyle);
		m_lableScore.setPosition(-((RunnerGame.WIDTH / 2) - 100), (RunnerGame.HEIGHT / 2) - 75);
		m_lableScore.setSize(100, 70);
		buttonCloseskin = new Skin(
				Gdx.files.internal("skins/buttonSkinClose.json"), new TextureAtlas(
				Gdx.files.internal("skins/buttonSkinClose.pack")));

		buttonClose = new TextButton("",buttonCloseskin);
		buttonClose.setSize(50, 50);
		buttonClose.setName("Close");
		buttonClose.setPosition((RunnerGame.WIDTH / 2) - 60, (RunnerGame.HEIGHT / 2) - 60);

		m_lIntinialScore = worldManager.getScore();




	}

	@Override
	public void render(float delta) {

		// Clear screen and set color to blue
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        long  score = worldManager.getScore();

		m_lableScore.setText(Long.toString(score));
		stage.act();
		stage.draw();
		boolean endGame = worldManager.draw();

		score_batch.setProjectionMatrix(score_Cam.combined);
		score_batch.begin();
		m_lableScore.draw(score_batch, 1);
		buttonClose.draw(score_batch,1);
		score_batch.end();


		if (!endGame) {
			int iCurrentScreen = 0;
			boolean boNewRecord = false;
			for (int iCnt = 0; iCnt < m_Game.m_GameMillestones.length; iCnt++) {
				if (score >= m_Game.m_GameMillestones[iCnt]) {
					iCurrentScreen = iCnt;
				}
			}
			if(m_lIntinialScore <m_Game.m_GameMillestones[iCurrentScreen] && score >=100)
			{
				boNewRecord = true;
			}
			if (boNewRecord) {
				m_Game.rewardScreen.setScreen(iCurrentScreen);
				m_Game.setScreen(m_Game.rewardScreen);
			} else {
				m_Game.setScreen(m_Game.splashScreen);
			}
		}
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height);

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {

		worldManager.dispose();
		stage.dispose();
		m_DefaultFont.dispose();
		buttonCloseskin.dispose();
		Gdx.input.setInputProcessor(null);
	}
	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {

		Vector3 worldCoordinates = stage.getViewport().getCamera().unproject(new Vector3(screenX,screenY,0));
		Gdx.app.log("un proyected", "touch done at ( x" + worldCoordinates.x + ",y " + worldCoordinates.y + ") + button: " + button);
		//buttonClose.setPosition((RunnerGame.WIDTH / 2) - 60, (RunnerGame.HEIGHT / 2) - 60);
		if((worldCoordinates.x > (RunnerGame.WIDTH /RunnerGame.PTM_RATIO) - 0.60)&&  (worldCoordinates.y >((RunnerGame.HEIGHT / RunnerGame.PTM_RATIO) - 0.60)))
		{
			worldManager.EndGame();
			m_Game.setScreen(m_Game.menuScreen);
		}
		worldManager.touchDown(worldCoordinates.x,  worldCoordinates.y);
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		worldManager.touchUp(screenX, screenY);
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
