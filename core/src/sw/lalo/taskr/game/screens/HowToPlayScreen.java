package sw.lalo.taskr.game.screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import sw.lalo.taskr.game.RunnerGame;

public class HowToPlayScreen implements Screen {
	
	private Stage stage ;
	private Music music;
	private RunnerGame m_Game;

	public HowToPlayScreen(RunnerGame game)
	{
		m_Game = game;
	}
	private Sprite back1;
	Camera score_Cam;
	SpriteBatch score_batch;

	private Skin buttonCloseskin;
	private TextButton buttonClose;
	@Override
	public void show() {
		stage = new Stage(new StretchViewport(RunnerGame.WIDTH, RunnerGame.HEIGHT));
		Gdx.input.setInputProcessor(stage);
		Gdx.input.setCatchBackKey(true);

		score_batch = new SpriteBatch();
		score_Cam = new OrthographicCamera(RunnerGame.WIDTH,
				RunnerGame.HEIGHT);
		score_Cam.position.set(0, 0, 0);
		score_Cam.update();

		back1 = new Sprite(new Texture("img/backgrounds/howToScreenArrow.png"));
		back1.setSize(RunnerGame.WIDTH, RunnerGame.HEIGHT);

		buttonCloseskin = new Skin(
				Gdx.files.internal("skins/buttonSkinClose.json"), new TextureAtlas(
				Gdx.files.internal("skins/buttonSkinClose.pack")));

		buttonClose = new TextButton("",buttonCloseskin);
		buttonClose.setSize(50, 50);
		buttonClose.setName("Close");
		buttonClose.setPosition((RunnerGame.WIDTH ) - 60, (RunnerGame.HEIGHT ) - 60);

		music = Gdx.audio.newMusic(Gdx.files.internal("audio/DanceAndJump.ogg"));
		music.setLooping(true);
		music.play();

		stage.addActor(buttonClose);
		buttonClose.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				m_Game.setScreen(m_Game.menuScreen);
				music.stop();
				music.dispose();

			}
		});

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		score_batch.setProjectionMatrix(score_Cam.combined);
		score_batch.begin();
		score_batch.draw(back1, -(RunnerGame.WIDTH/2.0f),-( RunnerGame.HEIGHT /2.0f), RunnerGame.WIDTH, RunnerGame.HEIGHT );
		score_batch.end();

		stage.act();
		stage.draw();
		if (Gdx.input.isKeyPressed(Input.Keys.BACK) || Gdx.input.isKeyPressed(Input.Keys.HOME)) {
			m_Game.setScreen(m_Game.menuScreen);
			music.stop();
			music.dispose();
		}
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void dispose() {
		stage.dispose();
	    score_batch.dispose();
		music.stop();
		music.dispose();
		buttonCloseskin.dispose();
	}
	
	@Override
	public void pause() {}

	@Override
	public void resume() {}

}
