package sw.lalo.taskr.game.screens;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import sw.lalo.taskr.game.RunnerGame;

public class MainMenuScreen implements Screen {

	private Stage stage ;
	private Skin m_ButtonPlaySkin ;
	private Skin m_ButtonAchievSkin ;
	private Skin m_ButtonHowToSkin ;
	private Skin buttonCloseskin;
	private TextButton buttonPlay ;
	private TextButton buttonAchieve;
	private TextButton buttonHowTo ;
	private TextButton buttonClose;

	private Preferences m_Preferences;
	private Long m_Score;
	private BitmapFont m_DefaultFont;
	private Label m_lableScore;
	private Sprite m_ScoreStar;
    private Sprite m_Title;
	Label.LabelStyle m_LabelStyle;
	Camera score_Cam;
	SpriteBatch score_batch;
	private Sprite back1;
	private Music music;
	private RunnerGame m_Game;


	public MainMenuScreen(RunnerGame game)
	{

		m_Game = game;
	}

	@Override
	public void show() {

		m_ButtonPlaySkin = new Skin(
				Gdx.files.internal("skins/buttonSkinPlay.json"), new TextureAtlas(
				Gdx.files.internal("skins/buttonSkinPlay.pack")));

		m_ButtonAchievSkin = new Skin(
				Gdx.files.internal("skins/buttonSkinAchieve.json"), new TextureAtlas(
				Gdx.files.internal("skins/buttonSkinAchieve.pack")));

		m_ButtonHowToSkin = new Skin(
				Gdx.files.internal("skins/buttonSkinHowTo.json"), new TextureAtlas(
				Gdx.files.internal("skins/buttonSkinHowTo.pack")));
		buttonCloseskin = new Skin(
				Gdx.files.internal("skins/buttonSkinClose.json"), new TextureAtlas(
				Gdx.files.internal("skins/buttonSkinClose.pack")));


		stage = new Stage(new FitViewport(RunnerGame.WIDTH, RunnerGame.HEIGHT));
		m_Preferences= Gdx.app.getPreferences("PreferencesScores");// We store the value 10 with the key of "highScore"
		m_Score = m_Preferences.getLong("Score", 0);
		m_ScoreStar = new Sprite(new Texture("img/items/coinGold.png"));
		m_ScoreStar.setSize(192, 192);
        m_Title = new Sprite(new Texture("img/snake_letter.png"));

		m_DefaultFont = new BitmapFont(Gdx.files.internal("fonts/gothic80.fnt"), Gdx.files.internal("fonts/gothic80.png"), false);
		m_DefaultFont.setColor(0, 0, 0, 1);
		m_LabelStyle = new Label.LabelStyle();
		m_LabelStyle.font = m_DefaultFont;
		m_lableScore = new Label(Long.toString(m_Score),m_LabelStyle);
		m_lableScore.setPosition(0, (RunnerGame.HEIGHT / 2) - 275);
        //m_lableScore.setPosition(-((RunnerGame.WIDTH / 2) - 250), (RunnerGame.HEIGHT / 2) - 275);
		m_lableScore.setSize(400, 100);
		score_batch = new SpriteBatch();
		score_Cam = new OrthographicCamera(RunnerGame.WIDTH,
				RunnerGame.HEIGHT);
		score_Cam.position.set(0, 0, 0);
		score_Cam.update();

		back1 = new Sprite(new Texture("img/backgrounds/blue_land.png"));
		back1.setSize(RunnerGame.WIDTH, RunnerGame.HEIGHT);

		buttonPlay = new TextButton("", m_ButtonPlaySkin);
		buttonAchieve = new TextButton("", m_ButtonAchievSkin);
		buttonHowTo = new TextButton("", m_ButtonHowToSkin);

		buttonPlay.setSize(200, 100);
		buttonAchieve.setSize(200, 100);
		buttonHowTo.setSize(200, 100);
		buttonClose = new TextButton("", buttonCloseskin);
		buttonClose.setSize(50, 50);
		buttonClose.setName("Close");
		buttonClose.setPosition((RunnerGame.WIDTH) - 60, (RunnerGame.HEIGHT) - 60);

		MenuListener menuListener = new MenuListener();
		buttonPlay.addListener(menuListener);
		buttonAchieve.addListener(menuListener);
		buttonHowTo.addListener(menuListener);
		buttonClose.addListener(menuListener);

		buttonPlay.setName("Play");
		buttonAchieve.setName("Achievements");
		buttonHowTo.setName("HowTo");

		Table table = new Table();
		table.padBottom(10);

		table.add(buttonPlay).size(200, 100).row();
		table.add(buttonAchieve).size(200,100).row();
		table.add(buttonHowTo).size(200, 100).row();

		table.setFillParent(true);
		table.bottom();
		music = Gdx.audio.newMusic(Gdx.files.internal("audio/DanceAndJump.ogg"));
		music.setLooping(true);
		music.play();
		stage.addActor(table);
		stage.addActor(buttonClose);
		Gdx.input.setInputProcessor(stage);
		Gdx.input.setCatchBackKey(true);

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		score_batch.setProjectionMatrix(score_Cam.combined);
		score_batch.begin();
		score_batch.draw(back1, -(RunnerGame.WIDTH / 2.0f), -(RunnerGame.HEIGHT / 2.0f), RunnerGame.WIDTH, RunnerGame.HEIGHT);
		score_batch.draw(m_ScoreStar, -150, (RunnerGame.HEIGHT / 2) - 310, 180, 180);
//score_batch.draw(m_ScoreStar, -((RunnerGame.WIDTH / 2) - 100), (RunnerGame.HEIGHT / 2) - 310, 180, 180);
        score_batch.draw(m_Title, -((RunnerGame.WIDTH / 2) -300 ), (RunnerGame.HEIGHT / 2) - 165, 350, 131);

        m_lableScore.draw(score_batch, 1);
		score_batch.end();

		stage.act(delta);
		stage.draw();



		if (Gdx.input.isKeyPressed(Keys.BACK) || Gdx.input.isKeyPressed(Keys.HOME)) {
			music.stop();
			music.dispose();
			Gdx.app.exit();

		}
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void dispose() {
		stage.dispose();
		score_batch.dispose();
		m_ButtonPlaySkin.dispose();
		m_ButtonAchievSkin.dispose();
		m_ButtonHowToSkin.dispose() ;
		music.stop();
		music.dispose();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
	private class MenuListener extends ChangeListener {
		@Override
		public void changed(ChangeEvent event, Actor actor) {

			if (actor.getName().equals("Play")) {
				music.stop();
				music.dispose();
				m_Game.setScreen(m_Game.gameScreen);


			}
			else if (actor.getName().equals("Achievements")) {
				music.stop();
				music.dispose();
				m_Game.rewardScreen.setScreen(0);
				m_Game.setScreen(m_Game.rewardScreen);

			}
			else if (actor.getName().equals("HowTo")) {
				music.stop();
				music.dispose();
				m_Game.setScreen(m_Game.HowToScreen);

			}
			else if (actor.getName().equals("Close")) {
				music.stop();
				music.dispose();
				Gdx.app.exit();

			}

		}
	}
}
