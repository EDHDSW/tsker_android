package sw.lalo.taskr.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import sw.lalo.taskr.game.RunnerGame;


public class RewardScreen implements Screen {

	private Stage stage ;
	private Music music;
	private RunnerGame m_Game;


	private Sprite back1;

	private Skin buttonCloseskin;
	private TextButton buttonClose;

	private Skin buttonPreviouSkin;
	private TextButton buttonPrevious;

	private Skin buttonNextSkin;
	private TextButton buttonNext;

	private BitmapFont m_DefaultFont;
	private Label m_lableScore;
	private Sprite m_ScoreStar;
	Label.LabelStyle m_LabelStyle;
	Camera score_Cam;
	SpriteBatch score_batch;
	int [] m_millestones = {100,200,400,800,1000,1300,1500,1800,2000,4000,5000,6000,8000,10000};
	private Sprite m_Achive;
	private Sprite m_NonAchive;






	String [] m_Sprites = {"img/animals/elephant.png",
			"img/Ground/Dirt/dirt.png",
			"img/animals/giraffe.png",
			"img/Ground/Planet/planet.png",
			"img/animals/hippo.png",
			"img/Ground/Sand/sand.png",
			"img/animals/monkey.png",
			"img/Ground/Snow/snow.png",
			"img/animals/panda.png",
			"img/Ground/Stone/stone.png",
			"img/animals/penguin.png",
			"img/animals/pig.png",
			"img/animals/rabbit.png",
			"img/animals/snake.png"};

	int currentScreen;
	boolean MillestoneReach;
	private Preferences m_Preferences;
	private Long m_Score;

	public void setScreen(int current)
	{

		currentScreen = current;
	}
	public RewardScreen(RunnerGame game)
	{

		currentScreen = 0;
		m_Game = game;
	}
	@Override
	public void show() {

		stage = new Stage(new StretchViewport(RunnerGame.WIDTH, RunnerGame.HEIGHT));

		m_Preferences= Gdx.app.getPreferences("PreferencesScores");// We store the value 10 with the key of "highScore"
		m_Score = m_Preferences.getLong("Score", 0);

		m_ScoreStar = new Sprite(new Texture("img/items/coinGold.png"));
		m_ScoreStar.setSize(192, 192);
		m_DefaultFont = new BitmapFont(Gdx.files.internal("fonts/gothic80.fnt"), Gdx.files.internal("fonts/gothic80.png"), false);
		m_DefaultFont.setColor(0, 0, 0, 1);
		m_LabelStyle = new Label.LabelStyle();
		m_LabelStyle.font = m_DefaultFont;
		m_lableScore = new Label(Integer.toString(m_millestones[0]), m_LabelStyle);
		m_lableScore.setPosition(-((RunnerGame.WIDTH / 2) - 450), (RunnerGame.HEIGHT / 2) - 160);
		m_lableScore.setSize(400, 100);
		score_batch = new SpriteBatch();
		score_Cam = new OrthographicCamera(RunnerGame.WIDTH,
				RunnerGame.HEIGHT);
		score_Cam.position.set(0, 0, 0);
		score_Cam.update();


		MillestoneReach = false;


		back1 = new Sprite(new Texture("img/backgrounds/blue_land.png"));
		back1.setSize(RunnerGame.WIDTH, RunnerGame.HEIGHT);

		buttonCloseskin = new Skin(
				Gdx.files.internal("skins/buttonSkinClose.json"), new TextureAtlas(
				Gdx.files.internal("skins/buttonSkinClose.pack")));

		buttonPreviouSkin = new Skin(
				Gdx.files.internal("skins/buttonSkinPrevious.json"), new TextureAtlas(
				Gdx.files.internal("skins/buttonSkinPrevious.pack")));

		buttonNextSkin = new Skin(
				Gdx.files.internal("skins/buttonSkinNext.json"), new TextureAtlas(
				Gdx.files.internal("skins/buttonSkinNext.pack")));

		buttonClose = new TextButton("",buttonCloseskin);
		buttonClose.setSize(50, 50);
		buttonClose.setName("Close");
		buttonClose.setPosition((RunnerGame.WIDTH) - 60, (RunnerGame.HEIGHT) - 60);

		buttonPrevious = new TextButton("",buttonPreviouSkin);
		buttonPrevious.setSize(100, 100);
		buttonPrevious.setName("Previous");
		buttonPrevious.setPosition(25, 30);

		buttonNext = new TextButton("",buttonNextSkin);
		buttonNext.setSize(100, 100);
		buttonNext.setName("Next");
		buttonNext.setPosition(RunnerGame.WIDTH - 125, 30);

		m_NonAchive = new Sprite(new Texture("img/animals/ic_lock_black_192.png"));
		m_Achive = new Sprite(new Texture(m_Sprites[currentScreen]));

		music = Gdx.audio.newMusic(Gdx.files.internal("audio/DanceAndJump.ogg"));
		music.setLooping(true);
		music.play();

		stage.addActor(buttonClose);
		stage.addActor(buttonPrevious);
		stage.addActor(buttonNext);

		buttonClose.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				m_Game.setScreen(m_Game.menuScreen);
				music.stop();
				music.dispose();

			}
		});

		buttonPrevious.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if(currentScreen>0) {
					currentScreen--;
					m_Achive = new Sprite(new Texture(m_Sprites[currentScreen]));
				}

			}
		});

		buttonNext.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if(currentScreen<13) {
					currentScreen++;
					m_Achive = new Sprite(new Texture(m_Sprites[currentScreen]));
				}
			}
		});
	//	stage.addListener(buttonClose.getClickListener());
		Gdx.input.setInputProcessor(stage);
		Gdx.input.setCatchBackKey(true);

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		score_batch.setProjectionMatrix(score_Cam.combined);

		score_batch.begin();
		score_batch.draw(back1, -(RunnerGame.WIDTH / 2.0f), -(RunnerGame.HEIGHT / 2.0f), RunnerGame.WIDTH, RunnerGame.HEIGHT);
		score_batch.draw(back1, -(RunnerGame.WIDTH / 2.0f), -(RunnerGame.HEIGHT / 2.0f), RunnerGame.WIDTH, RunnerGame.HEIGHT);
		score_batch.draw(m_ScoreStar, -((RunnerGame.WIDTH / 2) - 270), (RunnerGame.HEIGHT / 2) - 200, 192, 192);

		m_lableScore.setText(Integer.toString(m_millestones[currentScreen]));
		m_lableScore.draw(score_batch, 1);
		CheckMillestones();

		if(MillestoneReach)
		{

			score_batch.draw(m_Achive,- (m_Achive.getWidth()/2), -(m_Achive.getHeight()/2)-100, m_Achive.getWidth(), m_Achive.getHeight());

		}
		else {

			score_batch.draw(m_NonAchive,-(m_NonAchive.getWidth()/2),-(m_NonAchive.getHeight()/2)-25,192,192);
		}
		score_batch.end();

		stage.act();
		stage.draw();
		if (Gdx.input.isKeyPressed(Input.Keys.BACK) || Gdx.input.isKeyPressed(Input.Keys.HOME)) {
			m_Game.setScreen(m_Game.menuScreen);
			music.stop();
			music.dispose();
		}
	}
	void CheckMillestones()
	{

		if( m_Score > m_millestones[currentScreen]) {
			MillestoneReach = true;
		}
		else {
			MillestoneReach = false;
		}
	}
	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void dispose() {
		stage.dispose();
	    score_batch.dispose();
		music.stop();
		music.dispose();
		buttonCloseskin.dispose();
	}
	
	@Override
	public void pause() {}

	@Override
	public void resume() {}

}
