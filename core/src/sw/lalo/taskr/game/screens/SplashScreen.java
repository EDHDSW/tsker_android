package sw.lalo.taskr.game.screens;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.FitViewport;
import sw.lalo.taskr.game.RunnerGame;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class SplashScreen implements Screen {
    private Texture texture ;
    private Image splashImage;
    private Stage stage;
    private RunnerGame m_Game;
	public SplashScreen(RunnerGame game)
	{
		m_Game = game;
	}
	@Override
	public void show() {

		texture = new Texture("img/items/coinGold.png");
		splashImage = new Image(texture);
		stage = new Stage(new FitViewport(RunnerGame.WIDTH, RunnerGame.HEIGHT));
		stage.addActor(splashImage);
		splashImage.setColor(1, 1, 1, 0);
		splashImage.setPosition(RunnerGame.WIDTH/2 - splashImage.getWidth()/2, RunnerGame.HEIGHT/2 - splashImage.getHeight()/2);
		splashImage.addAction(Actions.sequence(fadeIn(1f), delay(1.5f), fadeOut(1f), run(new Runnable() {
            @Override
            public void run() {
				m_Game.setScreen(m_Game.menuScreen);
            }
        })));
	}

	@Override
	public void render(float delta) {
        Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
        stage.act(delta);
        stage.draw();
	}

	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void dispose() {
		texture.dispose();
		stage.dispose();
	}
	
	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}
	
	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
