package sw.lalo.taskr.calendar;


import android.app.Service;
import android.content.Context;
import android.content.Intent;

import sw.lalo.taskr.AndroidLauncher;
import sw.lalo.taskr.R;
import sw.lalo.taskr.calendar.interfaz.DatabaseTaskInterfaz;
import sw.lalo.taskr.calendar.interfaz.DatabaseUserInterfaz;
import sw.lalo.taskr.calendar.model.CStatistics;
import sw.lalo.taskr.calendar.model.CUser;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

public class AlarmReciever extends Service     {


    public void createNotification()
    {
        /*
        Intent intent = new Intent(this, Mainlist.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);
        String sMessage = getResources().getString(R.string.task_notification);
        android.support.v4.app.NotificationCompat.Builder builder =
                new android.support.v7.app.NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.stars)
                        .setContentTitle("Tasker")
                        .setContentText(sMessage);
        builder.setTicker( sMessage);
        builder.setContentIntent(pIntent);
        builder.setAutoCancel(true);


        builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE );
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        try {
            manager.notify(1, builder.build());
        }
        catch(Exception ex)
        {

        }*/
        Intent intent = new Intent(this, Mainlist.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, PendingIntent.FLAG_CANCEL_CURRENT);

        String sMessage = getResources().getString(R.string.task_notification);
        // Build notification
        // Actions are just fake
        Notification noti = new Notification.Builder(this)
                .setContentTitle("Tasker")
                .setContentText(sMessage).setSmallIcon(R.drawable.stars)
                .setContentIntent(pIntent).build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // hide the notification after its selected
        noti.flags |= Notification.FLAG_AUTO_CANCEL;
        try {

             notificationManager.notify(0, noti);

        }
        catch(Exception ex)
        {

        }
        Intent intent2 = new Intent(this, AlarmReciever.class);
        stopService(intent2);
    }
    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        Toast.makeText(this, R.string.task_notification, Toast.LENGTH_LONG).show();
        createNotification();
    }

    @Override
    public IBinder onBind(Intent intent) {
       // Toast.makeText(this, "MyAlarmService.onBind()", Toast.LENGTH_LONG).show();
        return null;
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
      //  Toast.makeText(this, "MyAlarmService.onDestroy()", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        // TODO Auto-generated method stub
        super.onStart(intent, startId);
       // Toast.makeText(this, "MyAlarmService.onStart()", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // TODO Auto-generated method stub
        //Toast.makeText(this, "MyAlarmService.onUnbind()", Toast.LENGTH_LONG).show();
        return super.onUnbind(intent);
    }




}
/*
public class AlarmReciever extends BroadcastReceiver{

    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        Log.i("lalo", "broadcasted");
        Toast.makeText(context, R.string.task_notification, Toast.LENGTH_LONG).show();

    }




}
*/


