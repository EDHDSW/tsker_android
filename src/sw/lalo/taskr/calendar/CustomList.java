package sw.lalo.taskr.calendar;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import sw.lalo.taskr.R;

import sw.lalo.taskr.calendar.model.CUser;
import sw.lalo.taskr.calendar.model.Ctask;

import java.util.ArrayList;


public class CustomList extends BaseAdapter implements ListAdapter {
    private ArrayList<String> list = new ArrayList<String>();

    ArrayList<Ctask> m_Taskslist = new ArrayList<Ctask>();
    private Context m_oContext;
    private Mainlist m_oMainlist = null;
    private CUser m_oWorkingUser = null;





/*
    public customlist(ArrayList<String> list, Context context) {
        this.list = list;
        this.context = context;

    }*/

    public CustomList(ArrayList<Ctask> list, Context context,Mainlist oMainThis,CUser oUser) {
        this.m_Taskslist = list;
        this.m_oContext = context;
        if (oMainThis != null)
        {
            this.m_oMainlist = oMainThis;
        }
        if(oUser != null)
        {
            m_oWorkingUser = oUser;
        }
    }
    public void  customlistAdd(Ctask entry) {
        this.m_Taskslist.add(entry);
        notifyDataSetChanged();

    }
    public void updateWorkingUser(CUser oUser)
    {
        if(oUser != null)
        {
            m_oWorkingUser = oUser;
        }
    }
    public void update() {
        notifyDataSetChanged();
    }
    /*
    public void  customlistAdd(String s) {
        this.list.add(s);
        notifyDataSetChanged();

    }*/

    @Override
    public int getCount() {
        return m_Taskslist.size();
    }

    @Override
    public Object getItem(int pos) {
        return m_Taskslist.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return 0;//list.get(pos).getId();
        //just return 0 if your list items do not have an Id variable.
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) m_oContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_entry, null);
        }

        //Handle TextView and display string from your list
        TextView listItemText = (TextView)view.findViewById(R.id.list_item_string);
        listItemText.setText(m_Taskslist.get(position).m_sTitle);

        TextView listItemDate = (TextView)view.findViewById(R.id.list_item_date);

        String sDate_show = Integer.toString(m_Taskslist.get(position).m_iStartHour) + ":" +
                Integer.toString(m_Taskslist.get(position).m_iStartMinute) + "-" +
                Integer.toString(m_Taskslist.get(position).m_iEndHour) + ":" +
                Integer.toString(m_Taskslist.get(position).m_iEndMinute);
        listItemDate.setText(sDate_show);
        //Handle buttons and add onClickListeners
        ImageView image = (ImageView)view.findViewById(R.id.imageView_item);

        switch(m_Taskslist.get(position).m_iStars)
        {
            case 1:
                if(m_Taskslist.get(position).getStatus() == false)
                {
                    image.setImageResource(R.drawable.star_icon_1_grey);
                }
                else
                {
                    image.setImageResource(R.drawable.star_icon_1);
                }
                break;
            case 2:
                if(m_Taskslist.get(position).getStatus() == false)
                {
                    image.setImageResource(R.drawable.star_icon_2_grey);
                }
                else
                {
                    image.setImageResource(R.drawable.star_icon_2);
                }
                break;
            case 3:
                if(m_Taskslist.get(position).getStatus() == false)
                {
                    image.setImageResource(R.drawable.star_icon_3_grey);
                }
                else
                {
                    image.setImageResource(R.drawable.star_icon_3);
                }
                break;
            case 4:
                if(m_Taskslist.get(position).getStatus() == false)
                {
                    image.setImageResource(R.drawable.star_icon_4_grey);
                }
                else
                {
                    image.setImageResource(R.drawable.star_icon_4);
                }
                break;
            case 5:
                if(m_Taskslist.get(position).getStatus() == false)
                {
                    image.setImageResource(R.drawable.star_icon_5_grey);
                }
                else
                {
                    image.setImageResource(R.drawable.star_icon_5);
                }
            break;
            default:
                image.setImageResource(R.drawable.star_icon_2);
                break;
        }

        view.setClickable(true);
        //listItemText.setClickable(true);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Ctask task = m_Taskslist.get(position);

                Intent intentBundle = new Intent(m_oMainlist, TaskDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("iTaskID",task.m_iID);
                bundle.putInt("iUserID",m_oWorkingUser.m_iID);
                intentBundle.putExtras(bundle);
                m_oMainlist.closeDatabase();
                m_oMainlist.startActivity(intentBundle);
                m_oMainlist.finish();
            }
        });
        return view;
    }
    public void UpdateList( ArrayList<Ctask> list) {
        this.m_Taskslist = list;
        notifyDataSetChanged();
    }
}
