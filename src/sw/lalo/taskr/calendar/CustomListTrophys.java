package sw.lalo.taskr.calendar;

import android.app.Dialog;
import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import sw.lalo.taskr.R;

import java.util.ArrayList;

import sw.lalo.taskr.calendar.model.CTrophy;

public class CustomListTrophys extends BaseAdapter implements ListAdapter {
    private ArrayList<CTrophy> m_TrophyList = new ArrayList<>();
    private Context m_Context;
    private TrophyList m_Mainlist = null;

    public CustomListTrophys(ArrayList<CTrophy> list, Context context, TrophyList oMainThis) {
        this.m_TrophyList = list;
        this.m_Context = context;
        this.m_Mainlist = oMainThis;
    }
    public void TrophylistAdd(CTrophy entry) {
        this.m_TrophyList.add(entry);
        notifyDataSetChanged();

    }
    @Override
    public int getCount() {
        return m_TrophyList.size();
    }

    @Override
    public Object getItem(int pos) {
        return m_TrophyList.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return 0;//list.get(pos).getId();
        //just return 0 if your list items do not have an Id variable.
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) m_Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.trophy_entry, null);
        }
        //TrophyList.get(position)
        TextView listItemText = (TextView)view.findViewById(R.id.tropyitem_title);
        listItemText.setText(m_TrophyList.get(position).m_iTitleID);

        ImageView userView = (ImageView)view.findViewById(R.id.imageView_trophy_item);
        userView.setImageResource(m_TrophyList.get(position).m_iIconID);




        return view;
    }
    public void UserListAdd(CTrophy oTrophy) {
        this.m_TrophyList.add(oTrophy);
        notifyDataSetChanged();

    }
    public void Update() {
        notifyDataSetChanged();

    }
    public void UpdateList(ArrayList<CTrophy> list) {
        this.m_TrophyList = list;
        notifyDataSetChanged();
    }
}