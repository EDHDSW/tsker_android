package sw.lalo.taskr.calendar;

import android.app.Dialog;
import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import sw.lalo.taskr.calendar.model.CUser;
import sw.lalo.taskr.R;
import java.util.ArrayList;

public class CustomListUsers extends BaseAdapter implements ListAdapter {
    private ArrayList<CUser> m_UserList = new ArrayList<>();
    private Context m_Context;
    private UserList m_Mainlist = null;

    public CustomListUsers(ArrayList<CUser> list, Context context,UserList oMainThis) {
        this.m_UserList = list;
        this.m_Context = context;
        this.m_Mainlist = oMainThis;
    }
    public void userlistAdd(CUser entry) {
        this.m_UserList.add(entry);
        notifyDataSetChanged();

    }
    @Override
    public int getCount() {
        return m_UserList.size();
    }

    @Override
    public Object getItem(int pos) {
        return m_UserList.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return 0;//list.get(pos).getId();
        //just return 0 if your list items do not have an Id variable.
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) m_Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.user_entry, null);
        }
        TextView listItemText = (TextView)view.findViewById(R.id.tropyitem_title);
        listItemText.setText(" " + m_UserList.get(position).m_sName);

        TextView listItemStarts = (TextView)view.findViewById(R.id.textview_staruseritem);

        listItemStarts.setText(Integer.toString(m_UserList.get(position).getTotalStars()));
        view.setClickable(true);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ((m_Mainlist != null) && (m_UserList.get(position).getStatus() == 0)) {
                    // find the active user
                    //Find Active user
                    boolean boActiveUserFound = false;
                    int iCnt = 0;
                    int iSize = m_UserList.size();
                    int iFoundIndex = 0;
                    while ((iCnt < iSize) && (boActiveUserFound == false)) {
                        if (m_UserList.get(iCnt).getStatus() > 0) {
                            m_UserList.get(iCnt).setStatus(0);
                        }
                        iCnt++;
                    }
                    m_UserList.get(position).setStatus(1);
                    m_Mainlist.SetUser(position);
                    notifyDataSetChanged();
                }
            }
        });
        /*//Handle TextView and display string from your list
        TextView listItemText = (TextView)view.findViewById(R.id.list_item_string);
        listItemText.setText(tasks_list.get(position).sTitle);*/

        ImageView userView = (ImageView)view.findViewById(R.id.imageView_trophy_item);

        if(m_UserList.get(position).getStatus() > 0)
        {
            userView.setImageResource(R.drawable.user);

        }
        else
        {
            userView.setImageResource(R.drawable.ic_face_white);

        }
        ImageView trash = (ImageView)view.findViewById(R.id.imageView_trash);

        trash.setClickable(true);
        trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //add simple confirmation pop-up

                if (m_Mainlist != null)
                {
                    final Dialog dialog = new Dialog(m_Context);
                    dialog.setContentView(R.layout.dialog_confirmation);
                    dialog.setTitle("Delete User");
                    TextView question = (TextView)dialog.findViewById(R.id.text_questionDialog);
                    question.setText("Do you want to delete the user ?");
                    Button deleteButton = (Button)dialog.findViewById(R.id.button_OKquestionDialog);
                    deleteButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            m_Mainlist.RemoveUser(position);
                            notifyDataSetChanged();
                            dialog.dismiss();
                        }
                    });
                    Button cancelButton = (Button)dialog.findViewById(R.id.button_CancelquestionDialog);
                    cancelButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();

                }
            }
        });
        return view;
    }
    public void UserListAdd(CUser oUser) {
        this.m_UserList.add(oUser);
        notifyDataSetChanged();

    }
    public void Update() {
        notifyDataSetChanged();

    }
    public void UpdateList(ArrayList<CUser> list) {
        this.m_UserList = list;
        notifyDataSetChanged();
    }
}