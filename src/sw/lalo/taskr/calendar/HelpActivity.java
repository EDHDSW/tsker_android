package sw.lalo.taskr.calendar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import sw.lalo.taskr.R;
import sw.lalo.taskr.calendar.interfaz.DatabaseUserInterfaz;

public class HelpActivity extends AppCompatActivity {

    //interfaces

    public static HelpActivity m_oInstance = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        m_oInstance =  this;

        //Action bar set up
        Toolbar myToolBar = (Toolbar) m_oInstance.findViewById(R.id.HelpToolbar);
        myToolBar.setTitle(R.string.help);
        setSupportActionBar(myToolBar);


            myToolBar.setNavigationIcon(R.drawable.arrow_back);
            myToolBar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        m_oInstance.startActivity(new Intent(m_oInstance, Mainlist.class));
                        m_oInstance.finish();
                    }
                    catch (Exception ex)
                    {


                    }
                }
            });





    }
    @Override
    public void onResume() {
        m_oInstance =this;
        super.onResume();
    }
    @Override
    public void onPause() {
        m_oInstance = null;
        super.onPause();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
}
