package sw.lalo.taskr.calendar;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View;

import sw.lalo.taskr.R;
import com.melnykov.fab.FloatingActionButton;


import android.widget.RatingBar;
import android.widget.TimePicker;

import sw.lalo.taskr.AndroidLauncher;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


import sw.lalo.taskr.calendar.interfaz.DatabaseTaskInterfaz;
import sw.lalo.taskr.calendar.interfaz.DatabaseUserInterfaz;
import sw.lalo.taskr.calendar.model.CStatistics;
import sw.lalo.taskr.calendar.model.CUser;
import sw.lalo.taskr.calendar.model.Ctask;

public class Mainlist extends AppCompatActivity {

    private CustomList adapter;
    private ArrayList<Ctask> m_tasklist;
    private DatabaseTaskInterfaz m_Database;

    private DatabaseUserInterfaz m_UsersDatabase;
    private ArrayList<CUser> m_UserList;
    private CUser m_oWorkingUser;
    private Menu m_oMenu;

    final Context context = this;
    public static Mainlist m_oInstance = null;
    private  ListView listView;

    private String m_sToday;
    private String m_sTomorrow;
    private String m_sWorkingDay;

    private boolean m_boTodayList = true ;

    private int m_nStarsPerGame = 3;

 //Interfaces

    public void openDatabase()
    {
        m_Database = new DatabaseTaskInterfaz(context);
        m_Database.open();
        m_UsersDatabase = new DatabaseUserInterfaz(context);
        m_UsersDatabase.open();
    }
    public void closeDatabase()
    {
        m_Database.close();
        m_UsersDatabase.close();
    }
    public void SetDates()
    {
         /*Get today & tomorrow Date*/
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        m_sToday = dateFormat.format(today);
        m_sTomorrow = dateFormat.format(tomorrow);
        m_sWorkingDay = m_sToday;

    }

    public void starNewUserActivity(boolean boCancel)
    {
        Intent intentBundle = new Intent(m_oInstance, NewUserActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean("boCancel", boCancel);
        intentBundle.putExtras(bundle);
        try {
            m_oInstance.closeDatabase();
            m_oInstance.startActivity(intentBundle);
            m_oInstance.finish();
        }
        catch (Exception ex)
        {

        }
    }
    public void ManageUsers()
    {
        boolean boActiveUserFound = false;
        m_UserList = m_UsersDatabase.getAllUsers();
        if (m_UserList.isEmpty())
        {
            CStatistics stat = new CStatistics(0, 0);
            m_oWorkingUser = new CUser("Craft", "default", "", 1, 0, stat,0,0,0,0,0);
           // starNewUserActivity(false);
            //CAMBIO USERLIST
            try {
                this.startActivity(new Intent(this, UserList.class));
                this.finish();
            }
            catch (Exception ex)
            {


            }
        }
        else {
            //Find Active user
            int iCnt = 0;
            int iSize = m_UserList.size();
            while ((iCnt < iSize) && (boActiveUserFound == false)) {
                if (m_UserList.get(iCnt).getStatus() > 0) {
                    m_oWorkingUser = new CUser(m_UserList.get(iCnt));
                    boActiveUserFound = true;
                }
                iCnt++;

            }
        }
        //If not active user found make the user 0 the current user
        if ((boActiveUserFound == false) && !(m_UserList.isEmpty()))
        {
            m_oWorkingUser = new CUser(m_UserList.get(0));
            m_UsersDatabase.ModifyStatus(m_oWorkingUser, 1);
        }
    }
    public void fillTaskList()
    {
        m_Database.deletePreviousTask(m_sToday, m_sTomorrow);
        m_tasklist =  m_Database.getTasksFromDay(m_sToday, m_oWorkingUser.m_sName);
    }

    @Override
    protected void onStop() {
        m_Database.close();
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }
    @Override
    public void onResume() {
        m_oInstance = this;
        openDatabase();
        super.onResume();

    }

    @Override
    public void onPause() {
        closeDatabase();
        super.onPause();
        m_oInstance = null;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        m_oInstance = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainlist);
        listView = (ListView) findViewById(R.id.mainlist);

        //start core
        openDatabase();
        SetDates();
        ManageUsers();
        fillTaskList();

        adapter = new CustomList(m_tasklist, context,m_oInstance,m_oWorkingUser);
        listView.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.attachToListView(listView);
        fab.setClickable(true);

        if (m_oWorkingUser.getStatus() > 1) {
            manageHideUnhide(false);
        }
        else
        {
            manageHideUnhide(true);
        }

        fab.setOnClickListener(new ListView.OnClickListener() {
            public void onClick(View v) {
                m_UsersDatabase.close();
                m_Database.close();

                Intent intentBundle = new Intent(m_oInstance, NewTaskActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("sDay", m_sWorkingDay);
                bundle.putInt("iUserID", m_oWorkingUser.m_iID);
                bundle.putString("sUserName", m_oWorkingUser.m_sName);
                intentBundle.putExtras(bundle);
                try {
                    closeDatabase();
                    m_oInstance.startActivity(intentBundle);
                    m_oInstance.finish();
                }
                catch (Exception ex)
                {


                }

            }
        });
        //Action bar set up
        Toolbar myToolBar = (Toolbar)this.findViewById(R.id.mainToolbar);
        myToolBar.setTitle(R.string.toolbar_today_title);
        setSupportActionBar(myToolBar);



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_mainlist, menu);
        MenuItem oItem = menu.findItem(R.id.menuItemLogin);
        if (m_oWorkingUser.getStatus() > 1)
        {
            oItem.setTitle(R.string.unLogin);
        }
        else
        {
            oItem.setTitle(R.string.Login);
        }
        MenuItem oConfig = menu.findItem(R.id.menuItemSettings);
        if (m_oWorkingUser.getStatus() > 1)
        {
            oConfig.setVisible(true);
        }
        else
        {
            oConfig.setVisible(false);
        }
        m_oMenu = menu;
        return super.onCreateOptionsMenu(menu);


    }


    private void ManageShowUserDetails()
    {
     /*create new dialog*/
        // custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.user_details);
        dialog.setTitle("User Details");
        TextView userName = (TextView)dialog.findViewById(R.id.text_nameUserDetails);
        TextView userStars= (TextView)dialog.findViewById(R.id.text_ViewStarsUserDetails);
        userName.setText(m_oWorkingUser.m_sName);
        userStars.setText(Integer.toString(m_oWorkingUser.getTotalStars()));

        Button dialogCancelButton = (Button) dialog.findViewById(R.id.button_CloseUserDetails);
        // if button is clicked, close the custom dialog
        dialogCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
    private void ManageChangeDays()
    {
        m_tasklist.clear();
        if(true == m_boTodayList) {
            m_tasklist = m_Database.getTasksFromDay(m_sTomorrow,m_oWorkingUser.m_sName);
            m_boTodayList = false;
            m_sWorkingDay = m_sTomorrow;
            adapter = new CustomList(m_tasklist, this,m_oInstance,m_oWorkingUser);
            listView.setAdapter(adapter);
            //Action bar set up
            Toolbar myToolBar = (Toolbar)this.findViewById(R.id.mainToolbar);
            myToolBar.setTitle(R.string.toolbar_tomorrow_title);
        }
        else {
            m_tasklist = m_Database.getTasksFromDay(m_sToday,m_oWorkingUser.m_sName);
            m_boTodayList= true;
            m_sWorkingDay = m_sToday;
            adapter = new CustomList(m_tasklist, this,m_oInstance,m_oWorkingUser);
            listView.setAdapter(adapter);
            //Action bar set up
            Toolbar myToolBar = (Toolbar)this.findViewById(R.id.mainToolbar);
            myToolBar.setTitle(R.string.toolbar_today_title);
        }

    }

    public void manageHideUnhide(boolean boHide)
    {

        if(boHide)
        {
            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setVisibility(View.GONE);
        }
        else
        {
            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setVisibility(View.VISIBLE);

        }
    }
    public void manageLogin()
    {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_loginuser);
       // dialog.setTitle("Login User: "+ m_oWorkingUser.m_sName);
        dialog.setTitle(R.string.dialog_login_title);
        Button dialogOKButton = (Button) dialog.findViewById(R.id.buttonOk_login);
        // if button is clicked, close the custom dialog
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get the values and create new user

                EditText password = (EditText) dialog.findViewById(R.id.editText_passwordLogin);

                String sPassword = password.getText().toString();
                if ( m_oWorkingUser.m_sPassword.equals(sPassword) )
                {
                    m_oWorkingUser.setStatus(2);
                    m_UsersDatabase.ModifyStatus(m_oWorkingUser, 2);
                    manageHideUnhide(false);
                    adapter.updateWorkingUser(m_oWorkingUser);
                    dialog.dismiss();

                }
                else
                {
                    TextView alert =(TextView)dialog.findViewById(R.id.textView_LoginAlert);
                    alert.setText(R.string.dialog_login_wrong);
                }


            }
        });
        Button dialogCancelButton = (Button)dialog.findViewById(R.id.buttonCancel_login);
        dialogCancelButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show(); //ask for the password

    }

    private void manageUnlogin()
    {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_unlogin);
        //dialog.setTitle("Un Login User: "+ m_oWorkingUser.m_sName);
        dialog.setTitle(R.string.dialog_unlogin_title);
        TextView text = (TextView)dialog.findViewById(R.id.textView_UnLogin);
        text.setText(R.string.dialog_unlogin_text);
        Button dialogOKButton = (Button) dialog.findViewById(R.id.buttonOk_Unlogin);
        // if button is clicked, close the custom dialog
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get the values and create new user
                m_oWorkingUser.setStatus(1);
                m_UsersDatabase.ModifyStatus(m_oWorkingUser, 1);
                manageHideUnhide(true);
                adapter.updateWorkingUser(m_oWorkingUser);
                dialog.dismiss();


            }
        });
        Button dialogCancelButton = (Button)dialog.findViewById(R.id.buttonCancel_Unlogin);
        dialogCancelButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }
    private void ManageGame()
    {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_unlogin);
        //dialog.setTitle("Un Login User: "+ m_oWorkingUser.m_sName);
        dialog.setTitle(R.string.dialog_init_game_title);
        TextView text = (TextView)dialog.findViewById(R.id.textView_UnLogin);
        text.setText(R.string.dialog_init_game);
        Button dialogOKButton = (Button) dialog.findViewById(R.id.buttonOk_Unlogin);
        // if button is clicked, close the custom dialog
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    //  closeDatabase();
                    m_UsersDatabase.ModifyStars(m_oWorkingUser, m_oWorkingUser.getStars() - m_nStarsPerGame);
                    m_oInstance.startActivity(new Intent(m_oInstance, AndroidLauncher.class));
                    //this.finish();
                }
                catch (Exception ex)
                {


                }
                dialog.dismiss();


            }
        });
        Button dialogCancelButton = (Button)dialog.findViewById(R.id.buttonCancel_Unlogin);
        dialogCancelButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();



    }
    private void RejectGame() {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_unlogin);
        //dialog.setTitle("Un Login User: "+ m_oWorkingUser.m_sName);
        dialog.setTitle(R.string.dialog_init_game_title);
        TextView text = (TextView)dialog.findViewById(R.id.textView_UnLogin);
        text.setText(R.string.dialog_init_game_reject);
        Button dialogOKButton = (Button) dialog.findViewById(R.id.buttonOk_Unlogin);
        // if button is clicked, close the custom dialog
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();


            }
        });
        Button dialogCancelButton = (Button)dialog.findViewById(R.id.buttonCancel_Unlogin);
        dialogCancelButton.setVisibility(View.GONE);
        dialogCancelButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    private void ManageTrophy()
    {

        try {
            Intent intentBundle = new Intent(this, TrophyList.class);
            Bundle bundle = new Bundle();
            bundle.putInt("iUserID",m_oWorkingUser.m_iID);
            intentBundle.putExtras(bundle);
            closeDatabase();
            this.startActivity(intentBundle);
            this.finish();
        }
        catch (Exception ex)
        {


        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId())
        {
            case R.id.menuItemUserDetails:
            {
                ManageTrophy();//ManageShowUserDetails();
                return true;
            }
            case R.id.menuItemToogleDay:
            {
                ManageChangeDays();
                return true;
            }
            case R.id.menuItemGames:
            {
                if(m_oWorkingUser.getStars() >2) {
                    ManageGame();
                }
                else {
                    RejectGame();
                }
                return true;
            }
          
            case R.id.menuItemLogin: {
                if(m_oWorkingUser.getStatus() > 1)
                {
                    manageUnlogin();
                    item.setTitle(R.string.Login);
                    MenuItem oConfig = m_oMenu.findItem(R.id.menuItemSettings);
                    oConfig.setVisible(false);
                }
                else {
                    manageLogin();
                    item.setTitle(R.string.unLogin);
                    MenuItem oConfig = m_oMenu.findItem(R.id.menuItemSettings);
                    oConfig.setVisible(true);
                }
                return true;
            }
            /*CAAMBIO USERLIST
            case R.id.menuItemUsers: {
                m_UsersDatabase.close();
                m_Database.close();
                this.startActivity(new Intent(this, UserList.class));
                this.finish();
                return true;
            }*/
            case R.id.menuItemSettings: {
                try {
                    m_UsersDatabase.close();
                    m_Database.close();
                    this.startActivity(new Intent(this, UserSettingsActivity.class));
                    this.finish();
                }
                catch (Exception ex)
                {


                }
                return true;
            }
            case R.id.menuItemHelp: {
                try {
                    m_UsersDatabase.close();
                    m_Database.close();
                    this.startActivity(new Intent(this, HelpActivity.class));
                    this.finish();
                }
                catch (Exception ex)
                {


                }
                return true;
            }

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

