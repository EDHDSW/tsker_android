package sw.lalo.taskr.calendar;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TimePicker;

import sw.lalo.taskr.calendar.interfaz.DatabaseTaskInterfaz;
import sw.lalo.taskr.calendar.interfaz.DatabaseUserInterfaz;
import sw.lalo.taskr.calendar.model.CStatistics;
import sw.lalo.taskr.calendar.model.CUser;
import sw.lalo.taskr.calendar.model.Ctask;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import sw.lalo.taskr.R;
public class NewTaskActivity extends AppCompatActivity {
    public static NewTaskActivity m_oInstance = null;
    private String m_sWorkingDay;
    private String m_Today;
    private String m_sUserName;
    private int m_iUserId;
    private DatabaseTaskInterfaz m_Database;
    private Calendar m_Calendar;
    //alarm manager objects
    Intent m_AlarmIntent;
    PendingIntent m_AlarmPendignIntent;
    AlarmManager m_AlarmManager;

    //Interfaces
    public void openDatabase()
    {
        m_Database = new DatabaseTaskInterfaz(m_oInstance);
        m_Database.open();
    }
    public void closeDatabase()
    {
        m_Database.close();
    }
    public void InitValues()
    {
         /*Get today & tomorrow Date*/
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        m_sWorkingDay = dateFormat.format(today);
        m_Today= m_sWorkingDay;
        m_iUserId = 0;
        m_sUserName ="default";
    }


    //Android
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Activity setup
        super.onCreate(savedInstanceState);
        m_oInstance = this;
        setContentView(R.layout.activity_new_task);

        //start Core
        openDatabase();
        InitValues();

        Intent intentExtras = getIntent();
        Bundle extrasBundle = intentExtras.getExtras();
        if(!extrasBundle.isEmpty())
        {
            if(extrasBundle.containsKey("iUserID"))
            {
                m_iUserId =extrasBundle.getInt("iUserID");
            }
            if(extrasBundle.containsKey("sDay"))
            {
                m_sWorkingDay =extrasBundle.getString("sDay");
            }
            if(extrasBundle.containsKey("sUserName"))
            {
                m_sUserName =extrasBundle.getString("sUserName");
            }
        }

        //Action bar set up
        Toolbar myToolBar = (Toolbar)this.findViewById(R.id.NewTaskToolbar);
        myToolBar.setTitle(R.string.toolbar_newtask_title);
        setSupportActionBar(myToolBar);
        myToolBar.setNavigationIcon(R.drawable.arrow_back);
        myToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    closeDatabase();
                    m_oInstance.startActivity(new Intent(m_oInstance, Mainlist.class));
                    m_oInstance.finish();
                }
                catch (Exception ex)
                {


                }
            }
        });

        //SET Control Properties
        TimePicker vTimePicker =(TimePicker)this.findViewById(R.id.timePickerTask_start);
        vTimePicker.setIs24HourView(true);
        TimePicker vTimePickerEnd =(TimePicker)this.findViewById(R.id.timePickerTask_end);
        vTimePickerEnd.setIs24HourView(true);
        RatingBar vRatingBar = (RatingBar) this.findViewById(R.id.ratingBarTask_edit);
        vRatingBar.setNumStars(5);
        Drawable drawable = vRatingBar.getProgressDrawable();
        drawable.setColorFilter(Color.parseColor("#FFe5c145"), PorterDuff.Mode.SRC_ATOP);

        //Listen to add button
        Button buttonAdd = (Button) this.findViewById(R.id.button_NewTaskAdd);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText vTask = (EditText) m_oInstance.findViewById(R.id.edit_task);
                String sTitle = vTask.getText().toString();

                RatingBar vRatingBar = (RatingBar) m_oInstance.findViewById(R.id.ratingBarTask_edit);
                int iRating = (int) vRatingBar.getRating();

                TimePicker vTimePickerStart = (TimePicker) m_oInstance.findViewById(R.id.timePickerTask_start);
                int iHourStart = vTimePickerStart.getCurrentHour();
                int iMinuteStart = vTimePickerStart.getCurrentMinute();

                TimePicker vTimePickerEnd = (TimePicker) m_oInstance.findViewById(R.id.timePickerTask_end);
                int iHourEnd = vTimePickerEnd.getCurrentHour();
                int iMinuteEnd = vTimePickerEnd.getCurrentMinute();
                CheckBox oCheckBoxNoti = (CheckBox)m_oInstance.findViewById(R.id.checkBox_enableNoti);
                boolean boAlarm =oCheckBoxNoti.isChecked();
                //Create in the database


                //  public Ctask createTask(String sList,String sTitle,String sDescription,int iYear,int iMonth,int iDay,int iStartHour
                // ,int iStartMinute,int iEndHour, int iEndMinute,int iStars,boolean boStatus,long iCalendarID)
                //m_oWorkingUser.m_sName;
                Ctask oEntry3 = m_Database.createTask(m_iUserId,m_sUserName, sTitle, m_sWorkingDay, iHourStart, iMinuteStart, iHourEnd, iMinuteEnd, iRating, false,boAlarm);

                //Alarm Service
                m_Calendar = Calendar.getInstance();
                if( !(m_sWorkingDay.equals(m_Today)) ) {//tomorrow

                    m_Calendar.add(Calendar.DAY_OF_YEAR, 1);
                }
                m_Calendar.set(Calendar.HOUR_OF_DAY, iHourStart);
                m_Calendar.set(Calendar.MINUTE, iMinuteStart);
                m_Calendar.set(Calendar.SECOND, 3);

                if( boAlarm)//set the alarm if required
                {

                     try {
                         m_AlarmIntent = new Intent(m_oInstance, AlarmReciever.class);
                         m_AlarmPendignIntent = PendingIntent.getService(m_oInstance, oEntry3.m_iID, m_AlarmIntent, PendingIntent.FLAG_CANCEL_CURRENT );
                         //m_AlarmPendignIntent = PendingIntent.getBroadcast(m_oInstance, 1234, m_AlarmIntent, PendingIntent.FLAG_UPDATE_CURRENT | Intent.FILL_IN_DATA);
                         m_AlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                          m_AlarmManager.set(AlarmManager.RTC_WAKEUP, m_Calendar.getTimeInMillis(), m_AlarmPendignIntent);

                     }
                     catch (Exception ex)
                     {


                     }

                }


                try {
                    closeDatabase();
                    m_oInstance.startActivity(new Intent(m_oInstance, Mainlist.class));
                    m_oInstance.finish();
                }
                catch (Exception ex)
                {


                }

            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        m_oInstance =this;
        openDatabase();
        super.onResume();
    }
    @Override
    public void onPause() {
        closeDatabase();
        m_oInstance = null;
        super.onPause();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_task, menu);
        return true;
    }


}
