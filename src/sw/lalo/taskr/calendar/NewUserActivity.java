package sw.lalo.taskr.calendar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import sw.lalo.taskr.R;
import sw.lalo.taskr.calendar.interfaz.DatabaseTaskInterfaz;
import sw.lalo.taskr.calendar.interfaz.DatabaseUserInterfaz;

public class NewUserActivity extends AppCompatActivity {

    //interfaces
    private DatabaseUserInterfaz m_UsersDatabase;
    public static NewUserActivity m_oInstance = null;
    private boolean m_boAllowCancelNewUser;
    public void openDatabase()
    {
        m_UsersDatabase = new DatabaseUserInterfaz(m_oInstance);
        m_UsersDatabase.open();
    }
    public void closeDatabase()
    {
        m_UsersDatabase.close();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);
        m_oInstance =  this;
        Intent intentExtras = getIntent();
        Bundle extrasBundle = intentExtras.getExtras();
        if(!extrasBundle.isEmpty())
        {
            if(extrasBundle.containsKey("boCancel"))
            {
                m_boAllowCancelNewUser  =extrasBundle.getBoolean("boCancel");
            }

        }
        //Action bar set up
        Toolbar myToolBar = (Toolbar) m_oInstance.findViewById(R.id.NewUserToolbar);
        myToolBar.setTitle(R.string.toolbar_newuser_title);
        setSupportActionBar(myToolBar);

        if(m_boAllowCancelNewUser) {

            myToolBar.setNavigationIcon(R.drawable.arrow_back);
            myToolBar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        closeDatabase();
                        m_oInstance.startActivity(new Intent(m_oInstance, UserList.class));
                        m_oInstance.finish();
                    }
                    catch (Exception ex)
                    {


                    }
                }
            });
        }



        Button AddButton = (Button) m_oInstance.findViewById(R.id.buttonOk_newuser);
        AddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                    //get the values and create new user
                    EditText user = (EditText) m_oInstance.findViewById(R.id.editText_newusername);
                    EditText password = (EditText) m_oInstance.findViewById(R.id.editText_password);
                    EditText confirmedPassword = (EditText) m_oInstance.findViewById(R.id.editText_confrrimpassword);

                    String sUser = user.getText().toString();
                    String sPassword = password.getText().toString();
                    String sConfirmedPassword = confirmedPassword.getText().toString();
                    if( (sPassword.isEmpty()) || (sConfirmedPassword.isEmpty())|| (sUser.isEmpty()) ) {
                        TextView alert =(TextView)m_oInstance.findViewById(R.id.textView_newUserAlert);
                        alert.setText(R.string.newuser_emptyinputs);
                    }

                    else if ( sConfirmedPassword.equals(sPassword)  )
                    {
                        if(m_boAllowCancelNewUser)// if are others user
                        {
                         //CAMBIO USERS   m_UsersDatabase.createUser(sUser, sPassword, sConfirmedPassword, 0);
                            m_UsersDatabase.createUser(sUser, sPassword, sConfirmedPassword, 2);

                        }
                        else {//if this is the first user set as administrator
                             m_UsersDatabase.createUser(sUser,sPassword, sConfirmedPassword, 2);
                        }
                        try {
                            closeDatabase();
                            m_oInstance.startActivity(new Intent(m_oInstance, Mainlist.class));
                            m_oInstance.finish();
                        }
                        catch (Exception ex)
                        {


                        }

                    }
                    else
                    {
                        TextView alert =(TextView)m_oInstance.findViewById(R.id.textView_newUserAlert);
                        alert.setText(R.string.newuser_notequal);

                    }


                }
            });
    }
    @Override
    public void onResume() {
        m_oInstance =this;
        openDatabase();
        super.onResume();
    }
    @Override
    public void onPause() {
        closeDatabase();
        m_oInstance = null;
        super.onPause();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
}
