package sw.lalo.taskr.calendar;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import sw.lalo.taskr.calendar.interfaz.DatabaseTaskInterfaz;
import sw.lalo.taskr.calendar.interfaz.DatabaseUserInterfaz;
import sw.lalo.taskr.calendar.model.CTrophy;
import sw.lalo.taskr.calendar.model.CUser;
import sw.lalo.taskr.calendar.model.Ctask;
import sw.lalo.taskr.R;
public class TaskDetailsActivity extends AppCompatActivity {


    public static TaskDetailsActivity m_oInstance = null;
    private DatabaseTaskInterfaz m_Database;
    private DatabaseUserInterfaz m_UsersDatabase;
    private int m_iTaskID;
    private int m_UserID;
    private Ctask m_oTask;
    private CUser m_oWorkingUser;


    //Interfaces
    public void openDatabase()
    {
        m_Database = new DatabaseTaskInterfaz(m_oInstance);
        m_Database.open();
        m_UsersDatabase = new DatabaseUserInterfaz(m_oInstance);
        m_UsersDatabase.open();
    }
    public void closeDatabase()
    {
        m_Database.close();
        m_UsersDatabase.close();
    }


    private boolean completeTask()
    {
        boolean iWait = false;
        if( m_oTask.getStatus() == false) {

            m_Database.CompleteTask(m_oTask);
            m_UsersDatabase.ModifyStats(m_oWorkingUser, m_oTask.m_iStars );
            iWait|= TrophyStars(m_oTask.m_iStars);
            if(m_oTask.m_iStars >= 5) {
                iWait|= TrophyFiveStars();
            }
            iWait|= TrophyTasks();
            iWait|=  BonificationStars(m_oTask.m_iStars);
            iWait|= BonificationTask();
        }
        return iWait;
    }
    private boolean TrophyStars(int iStars)
    {
        boolean iWait = false;
       int iTrophy = m_oWorkingUser.m_iTrophyStars + iStars;
       int iReached = -1;
       for(int cnt = 0; cnt <CTrophy.m_iTrophyStars.length;cnt++)
       {
           if(iTrophy >= CTrophy.m_iTrophyStars [cnt])
           {
               iReached = cnt;
           }
       }
        /*New trophy*/
        if((iReached>=0) && (m_oWorkingUser.m_iTrophyStars <CTrophy.m_iTrophyStars[iReached]) )
        {
            showPopUp(2);
            iWait = true;
        }
        m_UsersDatabase.ModifyTrophyStars(m_oWorkingUser,iTrophy);
        return iWait;
    }
    private boolean TrophyFiveStars()
    {
        boolean iWait = false;
        int iTrophy = m_oWorkingUser.m_iTrophyFiveStars + 1;
        int iReached =-1;
        for(int cnt = 0; cnt < CTrophy.m_iTrophyFiveStars.length;cnt++)
        {
            if(iTrophy >= CTrophy.m_iTrophyFiveStars [cnt])
            {
                iReached = cnt;
            }
        }
        /*New trophy*/
        if((iReached>=0) && (m_oWorkingUser.m_iTrophyFiveStars <CTrophy.m_iTrophyFiveStars[iReached] ))
        {
            showPopUp(3);
            iWait = true;
        }
        m_UsersDatabase.ModifyTrophyFiveStars(m_oWorkingUser,iTrophy);
        return iWait;
    }
    private boolean TrophyTasks()
    {
        boolean iWait = false;
        int iTrophy = m_oWorkingUser.m_iTrophyTasks + 1;
        int iReached =-1;
        for(int cnt = 0; cnt < CTrophy.m_iTrophyTasks.length;cnt++)
        {
            if(iTrophy >= CTrophy.m_iTrophyTasks [cnt])
            {
                iReached = cnt;
            }
        }
        /*New trophy*/
        if((iReached>=0) && (m_oWorkingUser.m_iTrophyTasks <CTrophy.m_iTrophyTasks[iReached] ))
        {
            showPopUp(4);
            iWait = true;
        }
        m_UsersDatabase.ModifyTrophyTasks(m_oWorkingUser,iTrophy);
        return iWait;
    }
    private boolean BonificationStars(int iStars)
    {
        boolean iWait = false;
        if(CTrophy.m_iBonificationStars <= (m_oWorkingUser.m_iBonificationStars+iStars) )
        {
            m_UsersDatabase.ResetBonificationStars(m_oWorkingUser);
            showPopUp(0);
            iWait = true;
        }
        return iWait;
    }
    private boolean BonificationTask()
    {
        boolean iWait = false;
        if(CTrophy.m_iBonificationTasks <= (m_oWorkingUser.m_iBonificationTasks +1))
        {
            m_UsersDatabase.ResetBonificationTasks(m_oWorkingUser);
            showPopUp(1);
            iWait = true;
        }
        return iWait;
    }
    private void showPopUp(int iBonification)
    {
        final Dialog dialog = new Dialog(m_oInstance);
        dialog.setContentView(R.layout.dialog_trophy);
        TextView congrats = (TextView)dialog.findViewById(R.id.textView_trophy_dialog);
        ImageView image = (ImageView)dialog.findViewById(R.id.imageView_trophy_dialog);

        switch( iBonification) {//stars
            case 0: //stars bonification
            {
                dialog.setTitle(R.string.bonus_stars_title);
                congrats.setText(R.string.bonus_congratulation);
                image.setImageResource(R.drawable.chest_open_rombo128);
                break;
            }

            case 1: // task bonification
            {
                dialog.setTitle(R.string.bonus_task_title);
                congrats.setText(R.string.bonus_congratulation);
                image.setImageResource(R.drawable.chest_open_rombo128);
                break;
            }
            case 2: // star trophy
            {
                dialog.setTitle(R.string.trophy_stars_title);
                congrats.setText(R.string.trophy_congratulation);
                image.setImageResource(R.drawable.v_feliz_circulo);
                break;
            }
            case 3: // five star trophy
            {
                dialog.setTitle(R.string.trophy_5stars_title);
                congrats.setText(R.string.trophy_congratulation);
                image.setImageResource(R.drawable.v_feliz_circulo);
                break;
            }
            case 4: // task trophy
            {
                dialog.setTitle(R.string.trophy_tasks_title);
                
                congrats.setText(R.string.trophy_congratulation);
                image.setImageResource(R.drawable.v_feliz_circulo);
                break;
            }
            case 5: // well done congratulation,not in use
            {
                dialog.setTitle(R.string.trophy_tasks_title);
                congrats.setText(R.string.trophy_congratulation);
                image.setImageResource(R.drawable.v_feliz_circulo);
                break;
            }
            default:
            {
                dialog.setTitle(R.string.bonus_task_title);
                congrats.setText(R.string.bonus_congratulation);
                image.setImageResource(R.drawable.chest_open_rombo128);
                break;
            }
        }




        Button dialogOkButton = (Button) dialog.findViewById(R.id.buttonOk_dialog_trophy);
        // if button is clicked, close the custom dialog
        dialogOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    private void deleteTask()
    {
        m_Database.deleteTask(m_oTask);

        Intent AlarmIntent = new Intent(m_oInstance, AlarmReciever.class);
        PendingIntent AlarmPendignIntent = PendingIntent.getService(m_oInstance, m_oTask.m_iID, AlarmIntent, 0 );
        //m_AlarmPendignIntent = PendingIntent.getBroadcast(m_oInstance, 1234, m_AlarmIntent, PendingIntent.FLAG_UPDATE_CURRENT | Intent.FILL_IN_DATA);
        AlarmManager AlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        AlarmManager.cancel(AlarmPendignIntent);
        AlarmPendignIntent.cancel();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);
        m_oInstance = this;

        m_iTaskID =0;

        openDatabase();
        Intent intentExtras = getIntent();
        Bundle extrasBundle = intentExtras.getExtras();
        if(!extrasBundle.isEmpty())
        {
            if(extrasBundle.containsKey("iTaskID"))
            {
                m_iTaskID =extrasBundle.getInt("iTaskID");
            }
            if(extrasBundle.containsKey("iUserID"))
            {
                m_UserID =extrasBundle.getInt("iUserID");
            }
        }
        m_oTask= m_Database.getTask(m_iTaskID);
        m_oWorkingUser = m_UsersDatabase.getUser(m_UserID);

        //Action Bar setup
        Toolbar myToolBar = (Toolbar)this.findViewById(R.id.TaskDetailToolbar);
        myToolBar.setTitle(R.string.toolbar_taskdetail_title);
        setSupportActionBar(myToolBar);
        myToolBar.setNavigationIcon(R.drawable.arrow_back);
        myToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDatabase();
                try {
                    m_oInstance.startActivity(new Intent(m_oInstance, Mainlist.class));
                    m_oInstance.finish();
                }
                catch (Exception ex)
                {


                }
            }
        });

        //View
        TextView sTitle = (TextView)m_oInstance.findViewById(R.id.text_ViewTitle);
        sTitle.setText(m_oTask.m_sTitle);


        TextView sDate = (TextView) m_oInstance.findViewById(R.id.Text_ViewDate);

                /*Format the date*/
        String sDate_dialog =
                Integer.toString(m_oTask.m_iStartHour) + ":" +
                        Integer.toString(m_oTask.m_iStartMinute) + "-" +
                        Integer.toString(m_oTask.m_iEndHour) + ":" +
                        Integer.toString(m_oTask.m_iEndMinute) +"   " +
                        m_oTask.m_sDate ;
        sDate.setText(sDate_dialog);

        RatingBar vRating = (RatingBar) m_oInstance.findViewById(R.id.ratingBar_view);
        Drawable drawable = vRating.getProgressDrawable();
        drawable.setColorFilter(Color.parseColor("#FFe5c145"), PorterDuff.Mode.SRC_ATOP);
        vRating.setNumStars(5);
        vRating.setRating(m_oTask.m_iStars);
        CheckBox oCheckBoxNoti = (CheckBox)m_oInstance.findViewById(R.id.checkBox_enableNoti_taskdetail);
        oCheckBoxNoti.setChecked(m_oTask.m_boAlarmSet);


        Button CompleteButton = (Button) m_oInstance.findViewById(R.id.button_ViewComplete);
        // if button is clicked, close the custom dialog
        CompleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(! completeTask()) {
                   try {
                       closeDatabase();
                       m_oInstance.startActivity(new Intent(m_oInstance, Mainlist.class));
                       m_oInstance.finish();
                   }
                   catch (Exception ex)
                   {

                   }
               }

            }
        });

        Button DeleteButton = (Button) m_oInstance.findViewById(R.id.button_ViewDelete);
        if (m_oWorkingUser.getStatus() < 2) {

            DeleteButton.setVisibility(View.GONE);
        } else {
            DeleteButton.setVisibility(View.VISIBLE);
        }
        // if button is clicked, close the custom dialog
        DeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    deleteTask();
                }
                catch (Exception ex)
                {


                }

                closeDatabase();
                try {
                    m_oInstance.startActivity(new Intent(m_oInstance, Mainlist.class));
                    m_oInstance.finish();
                }
                catch (Exception ex)
                {

                }
            }
        });

        if( m_oTask.getStatus() == true) {
            CompleteButton.setVisibility(View.INVISIBLE);
            DeleteButton.setVisibility(View.INVISIBLE);
        }

    }
    @Override
    public void onResume() {
        m_oInstance =this;
        openDatabase();
        super.onResume();
    }
    @Override
    public void onPause()
    {
        closeDatabase();
        m_oInstance = null;
        super.onPause();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_task_details, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*
        if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }
}
