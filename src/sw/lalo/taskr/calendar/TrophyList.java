package sw.lalo.taskr.calendar;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
//import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import sw.lalo.taskr.R;
import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.StringTokenizer;

import sw.lalo.taskr.calendar.interfaz.DatabaseTaskInterfaz;
import sw.lalo.taskr.calendar.interfaz.DatabaseUserInterfaz;
import sw.lalo.taskr.calendar.model.CStatistics;
import sw.lalo.taskr.calendar.model.CTrophy;
import sw.lalo.taskr.calendar.model.CUser;
import sw.lalo.taskr.calendar.model.Ctask;

public class TrophyList extends AppCompatActivity {
    private DatabaseUserInterfaz m_UsersDatabase;
    private ArrayList<CUser> m_UserList;
    private ArrayList<CTrophy> m_aTrophys;
    private CUser m_oWorkingUser;
    private int m_iUserID;


    private CustomListTrophys m_TrophysAdapter;
    private ListView m_ListView;
    public static TrophyList m_oInstance = null;
    final Context m_Context = this;

    //trophys images
    private static int [] m_iTrophysIconId = { R.drawable.vl_medalla1_rombo,R.drawable.az_medalla2_cuadro,  R.drawable.a_gusta_cuadro, R.drawable.a_corona_rombo,  R.drawable.vl_medalla6_cuadro,
                                                R.drawable.a_bandera_circulo, R.drawable.rc_trofeo_circulo , R.drawable.az_gusta_rombo, R.drawable.vl_medalla4_rombo, R.drawable.vl_medalla5_circulo,
                                                R.drawable.az_estrella_cuadro, R.drawable.a_corona_cuadro, R.drawable.az_lugares_circulo, R.drawable.az_medalla1_circulo, R.drawable.vl_medalla2_circulo,
                                                R.drawable.v_bandera_rombo, R.drawable.v_corona_circulo, R.drawable.vl_mono_rombo, R.drawable.vl_trofeo_circulo,R.drawable.a_escudo_circulo,
                                                R.drawable.rc_medalla3_rombo,R.drawable.ra_bandera_cuadro, R.drawable.rc_medalla6_cuadro, R.drawable.v_lugares_cuadro, R.drawable.v_gusta_circulo,
                                                R.drawable.a_medalla5_rombo, R.drawable.a_medalla6_circulo, R.drawable.az_bandera_circulo,R.drawable.v_bandera_rombo, R.drawable.az_escudo_circulo,
                                                R.drawable.az_medalla3_rombo, R.drawable.az_medalla4_circulo,  R.drawable.ra_estrella_cuadro, R.drawable.ra_gusta_rombo, R.drawable.az_trofeo_cuadro,
                                                R.drawable.rc_medalla5_cuadro, R.drawable.ra_corona_rombo, R.drawable.ra_escudo_circulo,R.drawable.az_medalla5_cuadro, R.drawable.az_medalla6_rombo,
                                                R.drawable.a_escudo_rombo, R.drawable.rc_bandera_cuadro, R.drawable.a_estrella_cuadro, R.drawable.vl_medalla3_cuadro, R.drawable.a_lugares_circulo,
                                                R.drawable.ra_lugares_circulo,  R.drawable.rc_estrella_circulo, R.drawable.rc_medalla1_rombo,R.drawable.a_estrella_circulo, R.drawable.rc_medalla2_cuadro,
                                                R.drawable.rc_mono_cuadro,R.drawable.a_bandera_cuadro, R.drawable.rc_trofeo_rombo,R.drawable.az_corona_rombo, R.drawable.v_corona_circulo,
                                                R.drawable.a_lugares_rombo, R.drawable.a_medalla1_cuadro, R.drawable.a_medalla2_rombo, R.drawable.a_medalla3_circulo, R.drawable.a_medalla4_cuadro

                                        };



    //trophy strings
    private static int [] m_iTrophysTitleId = {R.string.trophy_stars5, R.string.trophy_5stars1, R.string.trophy_task1, R.string.trophy_stars10, R.string.trophy_5stars5,
                                                R.string.trophy_task5, R.string.trophy_stars20, R.string.trophy_5stars10, R.string.trophy_task10, R.string.trophy_stars30,
                                                R.string.trophy_5stars20, R.string.trophy_task20, R.string.trophy_stars50, R.string.trophy_5stars30, R.string.trophy_task30,
                                                R.string.trophy_stars100, R.string.trophy_5stars50, R.string.trophy_task50, R.string.trophy_stars200, R.string.trophy_5stars100,
                                                R.string.trophy_task100, R.string.trophy_stars300, R.string.trophy_5stars200, R.string.trophy_task200, R.string.trophy_stars400,
                                                R.string.trophy_5star300, R.string.trophy_task300, R.string.trophy_stars500, R.string.trophy_5stars400, R.string.trophy_task400,
                                                R.string.trophy_stars700, R.string.trophy_5stars500, R.string.trophy_task500, R.string.trophy_stars800, R.string.trophy_5stars700,
                                                R.string.trophy_task700, R.string.trophy_stars1000, R.string.trophy_5stars1000, R.string.trophy_task1000, R.string.trophy_stars1200,
                                                R.string.trophy_5stars1200, R.string.trophy_task1200, R.string.trophy_stars1500, R.string.trophy_5stars1500, R.string.trophy_task1500,
                                                R.string.trophy_stars1700, R.string.trophy_5stars1700, R.string.trophy_task1700, R.string.trophy_stars2000, R.string.trophy_5stars2000,
                                                R.string.trophy_task2000, R.string.trophy_stars3000, R.string.trophy_5stars3000, R.string.trophy_task3000, R.string.trophy_stars5000,
                                                R.string.trophy_5stars5000, R.string.trophy_task5000, R.string.trophy_stars10000, R.string.trophy_5stars10000, R.string.trophy_task10000,
                                                 };

    //interfaces
    public void openDatabase()
    {
        m_UsersDatabase = new DatabaseUserInterfaz(m_Context);
        m_UsersDatabase.open();
    }
    public void closeDatabase()
    {
        m_UsersDatabase.close();
    }

//Android
    public void starNewUserActivity(boolean boCancel)
    {
        Intent intentBundle = new Intent(m_oInstance, NewUserActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean("boCancel", boCancel);
        intentBundle.putExtras(bundle);
        m_oInstance.closeDatabase();
        try {
            m_oInstance.startActivity(intentBundle);
            m_oInstance.finish();
        }
        catch (Exception ex)
        {


        }
    }

    public void setTrophys()
    {
        int iTotalLenght = CTrophy.m_iTrophyTotalLength;
         m_aTrophys = new ArrayList<>();
        int cnt20 = 0;
        int cnt60 = 0;
        while(cnt20 <20)
        {
            if(m_oWorkingUser.m_iTrophyStars >= CTrophy.m_iTrophyStars[cnt20])
            {
                m_aTrophys.add(new CTrophy(m_iTrophysTitleId[cnt60],m_iTrophysIconId[cnt60]));
            }
            else
            {
                m_aTrophys.add(new CTrophy(R.string.trophy_generic,R.drawable.g_trofeo_cuadro));
            }
            if(m_oWorkingUser.m_iTrophyFiveStars >= CTrophy.m_iTrophyFiveStars[cnt20])
            {
                m_aTrophys.add(new CTrophy(m_iTrophysTitleId[cnt60+1],m_iTrophysIconId[cnt60+1]));
            }
            else
            {
                m_aTrophys.add(new CTrophy(R.string.trophy_generic,R.drawable.g_trofeo_cuadro));
            }
            if(m_oWorkingUser.m_iTrophyTasks >= CTrophy.m_iTrophyTasks[cnt20])
            {
                m_aTrophys.add(new CTrophy(m_iTrophysTitleId[cnt60+2],m_iTrophysIconId[cnt60+2]));
            }
            else
            {
                m_aTrophys.add(new CTrophy(R.string.trophy_generic,R.drawable.g_trofeo_cuadro));
            }
            cnt20++;
            cnt60 = cnt60 +3;

        }

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trophy_list);
        m_ListView = (ListView) findViewById(R.id.trophylist);
        m_oInstance = this;
        //open the task database
        openDatabase();
       // ManageUsers();
        Intent intentExtras = getIntent();
        Bundle extrasBundle = intentExtras.getExtras();
        if(!extrasBundle.isEmpty())
        {
            if(extrasBundle.containsKey("iUserID"))
            {
                m_iUserID =extrasBundle.getInt("iUserID");
            }
        }
        m_oWorkingUser = m_UsersDatabase.getUser(m_iUserID);
        setTrophys();
        m_TrophysAdapter = new CustomListTrophys(m_aTrophys, m_Context,this);
        m_ListView.setAdapter(m_TrophysAdapter);
        //User
        TextView oName = (TextView)this.findViewById(R.id.text_nameUserDetails);
        TextView oStars = (TextView)this.findViewById(R.id.text_ViewStarsUserDetails);
        TextView oStarsTotal = (TextView)this.findViewById(R.id.text_View_total_StarsUserDetails);
        oName.setText(m_oWorkingUser.m_sName);
        oStars.setText(Integer.toString(m_oWorkingUser.getStars()));
        oStarsTotal.setText(Integer.toString(m_oWorkingUser.getTotalStars()));
        //Action bar set up
        Toolbar myToolBar = (Toolbar)this.findViewById(R.id.trophyToolbar);
        myToolBar.setTitle("Trophys");
        setSupportActionBar(myToolBar);
        myToolBar.setNavigationIcon(R.drawable.arrow_back);
        myToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                closeDatabase();
                try {
                    m_oInstance.startActivity(new Intent(m_oInstance, Mainlist.class));
                    m_oInstance.finish();
                }
                catch (Exception ex)
                {


                }
            }
        });

    }

    @Override
    protected void onStop() {
        closeDatabase();
        m_oInstance = null;
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }
    @Override
    public void onResume() {
        m_oInstance = this;
      //  openDatabase();
         super.onResume();

    }

    @Override
    public void onPause() {
        m_oInstance = null;
        //closeDatabase();
        super.onPause();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }
}

