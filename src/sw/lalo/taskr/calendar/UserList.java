package sw.lalo.taskr.calendar;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.melnykov.fab.FloatingActionButton;
import sw.lalo.taskr.calendar.interfaz.DatabaseTaskInterfaz;
import sw.lalo.taskr.calendar.interfaz.DatabaseUserInterfaz;
import sw.lalo.taskr.calendar.model.CStatistics;
import sw.lalo.taskr.calendar.model.CUser;

import sw.lalo.taskr.R;
import java.util.ArrayList;

public class UserList extends AppCompatActivity {
    private DatabaseUserInterfaz m_UsersDatabase;
    private DatabaseTaskInterfaz m_Database;
    private ArrayList<CUser> m_UserList;
    private CUser m_oWorkingUser;
    private CUser m_AuxiliarUser;
    private CustomListUsers m_UsersAdapter;
    private ListView m_DialogListView;
    public static UserList m_oInstance = null;
    final Context m_Context = this;
    boolean m_boNoUser = false;

    //interfaces
    public void openDatabase()
    {
        m_Database = new DatabaseTaskInterfaz(m_Context);
        m_Database.open();
        m_UsersDatabase = new DatabaseUserInterfaz(m_Context);
        m_UsersDatabase.open();
    }
    public void closeDatabase()
    {
        m_Database.close();
        m_UsersDatabase.close();
    }
    public void ManageUsers()
    {
        m_UserList = m_UsersDatabase.getAllUsers();
        boolean boActiveUserFound = false;

        if (m_UserList.isEmpty())
        {
            //set to new user pop-up
            //set new
            CStatistics stat = new CStatistics(0,0);
            m_oWorkingUser = new CUser("defaukt","default","",1,0,stat,0,0,0,0,0);
            //NewUserPopUp(true);
            starNewUserActivity(false);
        }
        else {
            //Find Active user
            int iCnt = 0;
            int iSize = m_UserList.size();
            while ((iCnt < iSize) && (boActiveUserFound == false)) {
                if (m_UserList.get(iCnt).getStatus() > 0) {
                    m_oWorkingUser = new CUser(m_UserList.get(iCnt));
                    boActiveUserFound = true;
                }
                iCnt++;

            }
        }
        //If not active user found make the user 0 the current user
        if ((boActiveUserFound == false) && !(m_UserList.isEmpty()))
        {
            m_oWorkingUser = new CUser(m_UserList.get(0));
            m_UsersDatabase.ModifyStatus(m_oWorkingUser, 1);
        }
    }
    public void SetUser(int iPosition) {

        m_UsersDatabase.ModifyStatus(m_oWorkingUser, 0);
        m_oWorkingUser = m_UserList.get(iPosition);
        m_oWorkingUser.setStatus(1);
        m_UsersDatabase.ModifyStatus(m_oWorkingUser, 1);

    }
    public void RemoveUser(int iPosition)
    {
        //remove only if not the working user
        if ( !(m_oWorkingUser.m_sName.equals(m_UserList.get(iPosition).m_sName))) {

            m_UsersDatabase.deleteUser(m_UserList.get(iPosition));
            m_UserList.remove(iPosition);
        }
        else {
            deleteAlertDialog();

        }
    }

//Android
    public void starNewUserActivity(boolean boCancel)
    {
        Intent intentBundle = new Intent(m_oInstance, NewUserActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean("boCancel", boCancel);
        intentBundle.putExtras(bundle);
        m_oInstance.closeDatabase();
        m_oInstance.startActivity(intentBundle);
        m_oInstance.finish();
    }
    public void deleteAlertDialog()
    {
        final Dialog dialog = new Dialog(m_Context);
        dialog.setContentView(R.layout.dialog_confirmation);
        dialog.setTitle("Warning");
        TextView question = (TextView)dialog.findViewById(R.id.text_questionDialog);
        question.setText("If not possible to remove the current user , please add another to delete this one");
        Button deleteButton = (Button)dialog.findViewById(R.id.button_OKquestionDialog);
        deleteButton.setVisibility(View.GONE);
        Button cancelButton = (Button)dialog.findViewById(R.id.button_CancelquestionDialog);
        cancelButton.setText("OK");
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        m_DialogListView = (ListView) findViewById(R.id.userslist);
        m_oInstance = this;
        //open the task database
        openDatabase();
        ManageUsers();

        // public CustomListUsers(ArrayList<CUser> list, Context context,mainlist oMainThis)
        m_UsersAdapter = new CustomListUsers(m_UserList, m_Context,this);
        m_DialogListView.setAdapter(m_UsersAdapter);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabUser);
        fab.attachToListView(m_DialogListView);
        fab.setClickable(true);
        fab.setOnClickListener(new ListView.OnClickListener() {
            public void onClick(View v) {

                starNewUserActivity(true);
            }
        });

        //Action bar set up
        Toolbar myToolBar = (Toolbar)this.findViewById(R.id.usersToolbar);
        myToolBar.setTitle("Users");
        setSupportActionBar(myToolBar);
        myToolBar.setNavigationIcon(R.drawable.arrow_back);
        myToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                closeDatabase();
                m_oInstance.startActivity(new Intent(m_oInstance, Mainlist.class));
                m_oInstance.finish();
            }
        });

    }

    @Override
    protected void onStop() {
        closeDatabase();
        m_oInstance = null;
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }
    @Override
    public void onResume() {
        m_oInstance = this;
      //  openDatabase();
         super.onResume();

    }

    @Override
    public void onPause() {
        m_oInstance = null;
        //closeDatabase();
        super.onPause();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }
}

