package sw.lalo.taskr.calendar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import sw.lalo.taskr.R;

import java.util.ArrayList;

import sw.lalo.taskr.calendar.interfaz.DatabaseUserInterfaz;
import sw.lalo.taskr.calendar.model.CStatistics;
import sw.lalo.taskr.calendar.model.CUser;

public class UserSettingsActivity extends AppCompatActivity {

    //interfaces
    private DatabaseUserInterfaz m_UsersDatabase;
    public static UserSettingsActivity m_oInstance = null;
    private ArrayList<CUser> m_UserList;
    private CUser m_oWorkingUser;

    public void openDatabase()
    {
        m_UsersDatabase = new DatabaseUserInterfaz(m_oInstance);
        m_UsersDatabase.open();
    }
    public void closeDatabase()
    {
        m_UsersDatabase.close();
    }
    public void ManageUsers()
    {
        m_UserList = m_UsersDatabase.getAllUsers();
        boolean boActiveUserFound = false;

        if (m_UserList.isEmpty())
        {
            CStatistics stat = new CStatistics(0,0);
            m_oWorkingUser = new CUser("defaukt","default","",1,0,stat,0,0,0,0,0);
        }
        else {
            //Find Active user
            int iCnt = 0;
            int iSize = m_UserList.size();
            while ((iCnt < iSize) && (boActiveUserFound == false)) {
                if (m_UserList.get(iCnt).getStatus() > 0) {
                    m_oWorkingUser = new CUser(m_UserList.get(iCnt));
                    boActiveUserFound = true;
                }
                iCnt++;

            }
        }
        //If not active user found make the user 0 the current user
        if ((boActiveUserFound == false) && !(m_UserList.isEmpty()))
        {
            m_oWorkingUser = new CUser(m_UserList.get(0));
            m_UsersDatabase.ModifyStatus(m_oWorkingUser, 1);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);
        m_oInstance =  this;
        openDatabase();
        ManageUsers();

            Toolbar myToolBar = (Toolbar) m_oInstance.findViewById(R.id.NewUserToolbar);
            myToolBar.setTitle(R.string.toolbar_usersettings_title);
            setSupportActionBar(myToolBar);
            myToolBar.setNavigationIcon(R.drawable.arrow_back);
            myToolBar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        closeDatabase();
                        m_oInstance.startActivity(new Intent(m_oInstance, Mainlist.class));
                        m_oInstance.finish();
                    }
                    catch (Exception ex)
                    {


                    }
                }
            });

        Button SaveButton = (Button) m_oInstance.findViewById(R.id.buttonOk_newuser);
        SaveButton.setText(R.string.save_user_settings_button);
        EditText Currentuser = (EditText) m_oInstance.findViewById(R.id.editText_newusername);
        Currentuser.setText(m_oWorkingUser.m_sName);

        SaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                    //get the values and create new user
                    EditText user = (EditText) m_oInstance.findViewById(R.id.editText_newusername);
                    EditText password = (EditText) m_oInstance.findViewById(R.id.editText_password);
                    EditText confirmedPassword = (EditText) m_oInstance.findViewById(R.id.editText_confrrimpassword);

                    String sUser = user.getText().toString();
                    String sPassword = password.getText().toString();
                    String sConfirmedPassword = confirmedPassword.getText().toString();
                    if((sPassword.isEmpty()) || (sConfirmedPassword.isEmpty()) || (sUser.isEmpty()) ) {
                        TextView alert =(TextView)m_oInstance.findViewById(R.id.textView_newUserAlert);
                        alert.setText(R.string.newuser_emptyinputs);
                    }

                    else if ( sConfirmedPassword.equals(sPassword)  )
                    {
                        m_UsersDatabase.ModifySettings(m_oWorkingUser, sUser, sPassword);
                        try {
                            closeDatabase();
                            m_oInstance.startActivity(new Intent(m_oInstance, Mainlist.class));
                            m_oInstance.finish();
                        }
                        catch (Exception ex)
                        {


                        }

                    }
                    else
                    {
                        TextView alert =(TextView)m_oInstance.findViewById(R.id.textView_newUserAlert);
                        alert.setText(R.string.newuser_notequal);

                    }

                }
            });
    }
    @Override
    public void onResume() {
        m_oInstance =this;
        openDatabase();
        super.onResume();
    }
    @Override
    public void onPause() {
        closeDatabase();
        m_oInstance = null;
        super.onPause();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
}
