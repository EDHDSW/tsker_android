package sw.lalo.taskr.calendar.interfaz;

/**
 * Created by Eduardo on 24/01/2016.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import sw.lalo.taskr.calendar.model.Ctask;
import sw.lalo.taskr.calendar.model.SQLConstructorTask;

import java.util.ArrayList;


public class DatabaseTaskInterfaz {

    public static final String DAY_ONE = "DAY1";
    public static final String DAY_TWO = "DAY2";
    private SQLiteDatabase m_database;
    private SQLConstructorTask m_dbHelper = null;
    private String[] m_asAllColumns ={SQLConstructorTask.COLUMN_ID,
            SQLConstructorTask.COLUMN_USERID,
            SQLConstructorTask.COLUMN_USER,
            SQLConstructorTask.COLUMN_TITLE,
            SQLConstructorTask.COLUMN_DATE,
            SQLConstructorTask.COLUMN_STARTHOUR,
            SQLConstructorTask.COLUMN_STARTMINUTE,
            SQLConstructorTask.COLUMN_ENDHOUR,
            SQLConstructorTask.COLUMN_ENDMINUTE,
            SQLConstructorTask.COLUMN_STARS,
            SQLConstructorTask.COLUMN_STATUS ,
            SQLConstructorTask.COLUMN_ALARM
    };
    public DatabaseTaskInterfaz(Context context) {

        m_dbHelper = new SQLConstructorTask(context);

    }
    public void open() throws SQLException {
        m_database = m_dbHelper.getWritableDatabase();

    }
    public void close()
    {
        m_dbHelper.close();
    }
    public Ctask createTask(int iUserID,String sUser,String sTitle,String sDate,int iStartHour
            ,int iStartMinute,int iEndHour, int iEndMinute,int iStars,boolean boStatus,boolean boAlarm)
    {
        ContentValues values = new ContentValues();

        int iStatus = 0;
        if(boStatus == true) {iStatus = 1;}
        int iAlarm = 0;
        if(boAlarm == true) {iAlarm = 1;}
        values.put(SQLConstructorTask.COLUMN_USERID, iUserID);
        values.put(SQLConstructorTask.COLUMN_USER, sUser);
        values.put(SQLConstructorTask.COLUMN_USER, sUser);
        values.put(SQLConstructorTask.COLUMN_TITLE, sTitle);
        values.put(SQLConstructorTask.COLUMN_DATE, sDate);
        values.put(SQLConstructorTask.COLUMN_STARTHOUR, iStartHour);
        values.put(SQLConstructorTask.COLUMN_STARTMINUTE, iStartMinute);
        values.put(SQLConstructorTask.COLUMN_ENDHOUR, iEndHour);
        values.put(SQLConstructorTask.COLUMN_ENDMINUTE, iEndMinute);
        values.put(SQLConstructorTask.COLUMN_STARS, iStars);
        values.put(SQLConstructorTask.COLUMN_STATUS, iStatus);
        values.put(SQLConstructorTask.COLUMN_ALARM, iAlarm);

        m_database.beginTransaction();
        long insertId = m_database.insert(SQLConstructorTask.TABLE_TASKS, null,
                values);
        m_database.setTransactionSuccessful();
        m_database.endTransaction();

        Cursor cursor = m_database.query(SQLConstructorTask.TABLE_TASKS,
                m_asAllColumns, SQLConstructorTask.COLUMN_ID + " = " + insertId, null,
                null, null, null);

        cursor.moveToFirst();
        Ctask oTask = cursorToCtask(cursor);
        cursor.close();

        return oTask;
    }
    public void deleteTask(Ctask oTask)
    {
        long id = oTask.m_iID;
        m_database.beginTransaction();
        m_database.delete(SQLConstructorTask.TABLE_TASKS, SQLConstructorTask.COLUMN_ID
                + " = " + id, null);
        m_database.setTransactionSuccessful();
        m_database.endTransaction();


    }
    public void CompleteTask(Ctask oTask) {
        long id = oTask.m_iID;
        int taskss= 0;
        ContentValues values = new ContentValues();
        m_database.beginTransaction();
        values.put(SQLConstructorTask.COLUMN_STATUS, true);
        taskss = m_database.update(SQLConstructorTask.TABLE_TASKS,values, SQLConstructorTask.COLUMN_ID + " IN (" + id +" )", null);
        m_database.setTransactionSuccessful();
        m_database.endTransaction();

    }
    public void deletePreviousTask(String sToday,String sTomorrow)
    {
        m_database.beginTransaction();
        m_database.delete(SQLConstructorTask.TABLE_TASKS, SQLConstructorTask.COLUMN_DATE
                + " != '" + sToday + "' AND " + SQLConstructorTask.COLUMN_DATE + " != '" + sTomorrow + "'", null);
        m_database.setTransactionSuccessful();
        m_database.endTransaction();
    }
    public void deleteUserTasks(String sUser)
    {
        m_database.beginTransaction();
        m_database.delete(SQLConstructorTask.TABLE_TASKS, SQLConstructorTask.COLUMN_USER
                + " = '" + sUser + "'", null);
        m_database.setTransactionSuccessful();
        m_database.endTransaction();
    }
    public Ctask getTask(int iID)
    {
        long id =iID;
        Cursor cursor = m_database.query(SQLConstructorTask.TABLE_TASKS,
                m_asAllColumns,SQLConstructorTask.COLUMN_ID + " = '" + iID + "'", null, null, null, null);//MySQLHelper.COLUMN_LIST + " = " + sList
        cursor.moveToFirst();
        Ctask oTask = cursorToCtask(cursor);
        cursor.close();
        return oTask;

    }
    public ArrayList<Ctask> getAllTask(String sUser)
    {
        ArrayList<Ctask> aTasks = new ArrayList<>();
        Cursor cursor = m_database.query(SQLConstructorTask.TABLE_TASKS,
                m_asAllColumns,SQLConstructorTask.COLUMN_USER + " = '" + sUser+"' ", null, null, null, "");//MySQLHelper.COLUMN_LIST + " = " + sList
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Ctask oTask = cursorToCtask(cursor);
            aTasks.add(oTask);
            cursor.moveToNext();
        }
        cursor.close();
        return aTasks;

    }
    public ArrayList<Ctask> getTasksFromDay(String sDate,String sUser)
    {
        ArrayList<Ctask> aTasks = new ArrayList<>();


        /*MySQLHelper.COLUMN_DATE
                + " != '" + sToday + "' AND " +  MySQLHelper.COLUMN_DATE + " != '" + sTomorrow + "'"*/
        Cursor cursor = m_database.query(SQLConstructorTask.TABLE_TASKS,
                m_asAllColumns,SQLConstructorTask.COLUMN_DATE + " = '" + sDate+"' AND " + SQLConstructorTask.COLUMN_USER + " = '" +sUser + "'", null, null, null, null);//MySQLHelper.COLUMN_LIST + " = " + sList
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Ctask oTask = cursorToCtask(cursor);
            aTasks.add(oTask);
            cursor.moveToNext();
        }
        cursor.close();
        return aTasks;

    }


    private Ctask cursorToCtask(Cursor cursor)
    {
        int iID = cursor.getInt(0);
        String sTitle = cursor.getString(3);
        String sDate = cursor.getString(4);
        int iStartHour = cursor.getInt(5);
        int iStartMinute = cursor.getInt(6);
        int iEndHour = cursor.getInt(7);
        int iEndMinute = cursor.getInt(8);
        int iStars = cursor.getInt(9);
        int iStatus = cursor.getInt(10);
        boolean boStatus = false;
        if(iStatus>0) {boStatus = true;}

        int iAlarm = cursor.getInt(11);
        boolean boAlarm = false;
        if(iAlarm>0) {boAlarm = true;}
        /*public Ctask(String sTitle,String sDescription,int iYear,int iMonth,int iDay,int iStartHour
            ,int iStartMinute,int iEndHour, int iEndMinute,int iStars,boolean boStatus,int iID,long iCalendarID)
            (String sTitle,String sDescription,int iYear,int iMonth,int iDay,int iStartHour
            ,int iStartMinute,int iEndHour, int iEndMinute,int iStars,boolean boStatus,int iID,long iCalendarID)
            */
        return new Ctask(sTitle,sDate,iStartHour,iStartMinute,iEndHour,iEndMinute,
                iStars,boStatus,iID,boAlarm);

    }


}

