package sw.lalo.taskr.calendar.interfaz;

/**
 * Created by Eduardo on 24/01/2016.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import sw.lalo.taskr.calendar.model.CStatistics;
import sw.lalo.taskr.calendar.model.CUser;
import sw.lalo.taskr.calendar.model.SQLConstructorUsers;

import java.util.ArrayList;

public class DatabaseUserInterfaz
{

    private SQLiteDatabase m_database;
    private SQLConstructorUsers m_dbHelper;
    private String[] m_asAllColumns ={SQLConstructorUsers.COLUMN_ID,
            SQLConstructorUsers.COLUMN_NAME,
            SQLConstructorUsers.COLUMN_PASSWORD,
            SQLConstructorUsers.COLUMN_STATUS,
            SQLConstructorUsers.COLUMN_STARS,
            SQLConstructorUsers.COLUMN_TOTALSTARS,
            SQLConstructorUsers.COLUMN_PREFERENCES,
            SQLConstructorUsers.COLUMN_TROPHYSTARS,
            SQLConstructorUsers.COLUMN_TROPHYFIVESTARS,
            SQLConstructorUsers.COLUMN_TROPHYTASKS,
            SQLConstructorUsers.COLUMN_BONIFICATIONSTARS,
            SQLConstructorUsers.COLUMN_BONIFICATIONTASKS
    };
    public DatabaseUserInterfaz(Context context) {
        m_dbHelper = new SQLConstructorUsers(context);
    }
    public void open() throws SQLException {
        m_database = m_dbHelper.getWritableDatabase();

    }
    public void close()
    {
        m_dbHelper.close();
    }

    public CUser createUser(String sName,String sPassword,String sPreferences,int iStatus)
    {
        ContentValues values = new ContentValues();

        values.put(SQLConstructorUsers.COLUMN_NAME, sName);
        values.put(SQLConstructorUsers.COLUMN_PASSWORD, sPassword);
        values.put(SQLConstructorUsers.COLUMN_STATUS, iStatus);
        values.put(SQLConstructorUsers.COLUMN_STARS, 0);
        values.put(SQLConstructorUsers.COLUMN_TOTALSTARS, 0);
        values.put(SQLConstructorUsers.COLUMN_PREFERENCES, sPreferences);
        values.put(SQLConstructorUsers.COLUMN_TROPHYSTARS, 0);
        values.put(SQLConstructorUsers.COLUMN_TROPHYFIVESTARS, 0);
        values.put(SQLConstructorUsers.COLUMN_TROPHYTASKS, 0);
        values.put(SQLConstructorUsers.COLUMN_BONIFICATIONSTARS, 0);
        values.put(SQLConstructorUsers.COLUMN_BONIFICATIONTASKS, 0);

        m_database.beginTransaction();
        long insertId = m_database.insert(SQLConstructorUsers.TABLE_USERS, null,
                values);
        m_database.setTransactionSuccessful();
        m_database.endTransaction();

        Cursor cursor = m_database.query(SQLConstructorUsers.TABLE_USERS,
                m_asAllColumns, SQLConstructorUsers.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        CUser oUser = cursorToCUser(cursor);



        return oUser;
    }
    public void deleteUser(CUser oUser)
    {
        long id = oUser.m_iID;
        m_database.beginTransaction();
        m_database.delete(SQLConstructorUsers.TABLE_USERS, SQLConstructorUsers.COLUMN_ID
                + " = " + id, null);
        m_database.setTransactionSuccessful();
        m_database.endTransaction();

    }
    public void ModifySettings(CUser oUser,String sName, String sPassword)
    {
        long id = oUser.m_iID;
        int users = 0;
        ContentValues values = new ContentValues();
        m_database.beginTransaction();
        values.put(SQLConstructorUsers.COLUMN_NAME,  sName);
        values.put(SQLConstructorUsers.COLUMN_PASSWORD,  sPassword);
        users = m_database.update(SQLConstructorUsers.TABLE_USERS,values, SQLConstructorUsers.COLUMN_ID + " IN (" + id +" )", null);
        m_database.setTransactionSuccessful();
        m_database.endTransaction();
    }
    public void ModifyStars(CUser oUser,int iStars) {
        long id = oUser.m_iID;
        int users = 0;
        ContentValues values = new ContentValues();
        m_database.beginTransaction();
        values.put(SQLConstructorUsers.COLUMN_STARS, iStars);
        users = m_database.update(SQLConstructorUsers.TABLE_USERS,values, SQLConstructorUsers.COLUMN_ID + " IN (" + id +" )", null);
        m_database.setTransactionSuccessful();
        m_database.endTransaction();

    }
    public void ModifyStats(CUser oUser,int iStars) {
        long id = oUser.m_iID;
        int users = 0;
        ContentValues values = new ContentValues();
        m_database.beginTransaction();
        values.put(SQLConstructorUsers.COLUMN_STARS,(oUser.getStars() + iStars));
        values.put(SQLConstructorUsers.COLUMN_TOTALSTARS, (oUser.getTotalStars() + iStars ));
        values.put(SQLConstructorUsers.COLUMN_BONIFICATIONSTARS, (oUser.m_iBonificationStars +iStars));
        values.put(SQLConstructorUsers.COLUMN_BONIFICATIONTASKS,(oUser.m_iBonificationTasks+1));
        users = m_database.update(SQLConstructorUsers.TABLE_USERS,values, SQLConstructorUsers.COLUMN_ID + " IN (" + id +" )", null);
        m_database.setTransactionSuccessful();
        m_database.endTransaction();

    }
    public void ResetBonificationStars(CUser oUser) {
        long id = oUser.m_iID;
        int users = 0;
        ContentValues values = new ContentValues();
        m_database.beginTransaction();
        values.put(SQLConstructorUsers.COLUMN_BONIFICATIONSTARS, 0);
        users = m_database.update(SQLConstructorUsers.TABLE_USERS,values, SQLConstructorUsers.COLUMN_ID + " IN (" + id +" )", null);
        m_database.setTransactionSuccessful();
        m_database.endTransaction();

    }
    public void ResetBonificationTasks(CUser oUser) {
        long id = oUser.m_iID;
        int users = 0;
        ContentValues values = new ContentValues();
        m_database.beginTransaction();
        values.put(SQLConstructorUsers.COLUMN_BONIFICATIONTASKS, 0);
        users = m_database.update(SQLConstructorUsers.TABLE_USERS,values, SQLConstructorUsers.COLUMN_ID + " IN (" + id +" )", null);
        m_database.setTransactionSuccessful();
        m_database.endTransaction();

    }
    public void ModifyTrophyStars(CUser oUser,int iTrophy) {
        long id = oUser.m_iID;
        int users = 0;
        ContentValues values = new ContentValues();
        m_database.beginTransaction();
        values.put(SQLConstructorUsers.COLUMN_TROPHYSTARS, iTrophy);
        users = m_database.update(SQLConstructorUsers.TABLE_USERS,values, SQLConstructorUsers.COLUMN_ID + " IN (" + id +" )", null);
        m_database.setTransactionSuccessful();
        m_database.endTransaction();

    }
    public void ModifyTrophyFiveStars(CUser oUser,int iTrophy) {
        long id = oUser.m_iID;
        int users = 0;
        ContentValues values = new ContentValues();
        m_database.beginTransaction();
        values.put(SQLConstructorUsers.COLUMN_TROPHYFIVESTARS, iTrophy);
        users = m_database.update(SQLConstructorUsers.TABLE_USERS,values, SQLConstructorUsers.COLUMN_ID + " IN (" + id +" )", null);
        m_database.setTransactionSuccessful();
        m_database.endTransaction();

    }
    public void ModifyTrophyTasks(CUser oUser,int iTrophy) {
        long id = oUser.m_iID;
        int users = 0;
        ContentValues values = new ContentValues();
        m_database.beginTransaction();
        values.put(SQLConstructorUsers.COLUMN_TROPHYTASKS, iTrophy);
        users = m_database.update(SQLConstructorUsers.TABLE_USERS,values, SQLConstructorUsers.COLUMN_ID + " IN (" + id +" )", null);
        m_database.setTransactionSuccessful();
        m_database.endTransaction();

    }
    public void ModifyStatus(CUser oUser,int iStatus) {
        long id = oUser.m_iID;
        int users = 0;
        ContentValues values = new ContentValues();
        m_database.beginTransaction();
        values.put(SQLConstructorUsers.COLUMN_STATUS, iStatus);
        users = m_database.update(SQLConstructorUsers.TABLE_USERS,values, SQLConstructorUsers.COLUMN_ID + " IN (" + id +" )", null);
        m_database.setTransactionSuccessful();
        m_database.endTransaction();

    }
    public CUser getUser(int iID)
    {
        ArrayList<CUser> aUsers = new ArrayList<CUser>();
        Cursor cursor = m_database.query(SQLConstructorUsers.TABLE_USERS,
                m_asAllColumns, SQLConstructorUsers.COLUMN_ID + " = '" + iID + "'", null, null, null, null);//MySQLHelper.COLUMN_LIST + " = " + sList
        cursor.moveToFirst();
        CUser oUser = cursorToCUser(cursor);

        cursor.close();
        return oUser;
    }

    public ArrayList<CUser> getAllUsers()
    {
        ArrayList<CUser> aUsers = new ArrayList<CUser>();
        Cursor cursor = m_database.query(SQLConstructorUsers.TABLE_USERS,
                m_asAllColumns,null, null, null, null, null);//MySQLHelper.COLUMN_LIST + " = " + sList
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            CUser oUser = cursorToCUser(cursor);
            aUsers.add(oUser);
            cursor.moveToNext();
        }
        cursor.close();
        return aUsers;

    }

    private CUser cursorToCUser(Cursor cursor)
    {
        int iID = cursor.getInt(0);
        String sName = cursor.getString(1);
        String sPassword = cursor.getString(2);
        int iStatus =  cursor.getInt(3);
        int iStars = cursor.getInt(4);
        int iTotalStars = cursor.getInt(5);
        String sPreferences = cursor.getString(6);
        int iTrophyStars = cursor.getInt(7);
        int iTrophyFiveStars = cursor.getInt(8);
        int iTrophyTasks = cursor.getInt(9);
        int iBonificationStars = cursor.getInt(10);
        int iBonificationTasks = cursor.getInt(11);


        /* public CUser(String sName,String sPassword, String sPreferences,int iStatus,int iID, CStatistics oStatics)*/
        CUser oUser = new CUser(sName,sPassword,sPreferences,iStatus,iID,new CStatistics(iStars,iTotalStars),iTrophyStars,
                iTrophyFiveStars,iTrophyTasks,iBonificationStars,iBonificationTasks);
        return oUser;
    }


}
