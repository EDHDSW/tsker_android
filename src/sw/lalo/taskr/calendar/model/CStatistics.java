package sw.lalo.taskr.calendar.model;

/**
 * Created by Eduardo on 24/01/2016.
 */
public class
CStatistics {
    public int m_iStars; /*stars to spend */
    public int m_iTotalStars;/*all the stars*/
    public CStatistics ()
    {

    }
    public CStatistics (int iStars,int iTotalStars)
    {
        this.m_iStars = iStars;
        this.m_iTotalStars = iTotalStars;

    }
    public CStatistics (CStatistics oStatistics)
    {
        this.m_iStars = oStatistics.m_iStars;
        this.m_iTotalStars = oStatistics.m_iTotalStars;
    }
    public int  getStars()
    {
        return m_iStars;

    }
    public int getTotalStars()
    {
      return m_iTotalStars;
    }
    public void setStars(int stars)
    {
        m_iStars = stars;

    }
    public void setTotalStars(int stars)
    {
        m_iTotalStars = stars;
    }
}
