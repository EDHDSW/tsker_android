package sw.lalo.taskr.calendar.model;

/**
 * Created by Eduardo on 24/01/2016.
 */

/**
 * Created by Eduardo on 02/11/2015.
 */
public class CTrophy {
    public int m_iTitleID;
    public int m_iIconID;
    public static int m_iBonificationTasks = 5;
    public static int m_iBonificationStars = 15;

    public static int[] m_iTrophyStars = {5,10,20,30,50,100,200,300,400,500,700,800,1000,1200,1500,1700,2000,3000,5000,10000};//120
    public static int[] m_iTrophyFiveStars = {1,5,10,20,30,50,100,200,300,400,500,700,1000,1200,1500,1700,2000,3000,5000,10000};//20
    public static int[] m_iTrophyTasks = {1,5,10,20,30,50,100,200,300,400,500,700,1000,1200,1500,1700,2000,3000,5000,10000};//20
    public static int m_iTrophyTotalLength = 60;



    public CTrophy(int iTitleID, int iIconID)
    {
        this.m_iTitleID = iTitleID;
        this.m_iIconID = iIconID;

    }

    public CTrophy()
    {
        this.m_iTitleID = 0;
        this.m_iIconID =0;
    }
    public void fillFromJASON()
    {

    }



}
