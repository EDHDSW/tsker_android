package sw.lalo.taskr.calendar.model;

/**
 * Created by Eduardo on 24/01/2016.
 */
public class CUser {
    public String m_sName;
    public String m_sPassword;
    public String m_sPreferences;
    private int m_iStatus;//0 user,1 working user, 2 working user &administrator
    public int m_iID;
    public int m_iTrophyStars;
    public int m_iTrophyFiveStars;
    public int m_iTrophyTasks;
    public int m_iBonificationStars;
    public int m_iBonificationTasks;


    /*Estadisticas*/
    private CStatistics oStatics;

    public CUser(String sName,String sPassword, String sPreferences,int iStatus,int iID, CStatistics oStatics,int iTrophyStars,int iTrophyFiveStars,int iTrophyTasks,
                 int iBonificationStars, int iBonificationTasks)
    {
        this.m_sName = sName;
        this.m_sPassword = sPassword;
        this.m_sPreferences = sPreferences;
        this.m_iStatus = iStatus;
        this.m_iID = iID;
        this.oStatics = new CStatistics(oStatics);
        this.m_iTrophyStars = iTrophyStars;
        this.m_iTrophyFiveStars = iTrophyFiveStars;
        this.m_iTrophyTasks = iTrophyTasks;
        this.m_iBonificationStars = iBonificationStars;
        this.m_iBonificationTasks = iBonificationTasks;

    }
    public CUser() {

    }
    public CUser(CUser oUser)
    {
        this.m_sName = oUser.m_sName;
        this.m_sPassword = oUser.m_sPassword;
        this.m_sPreferences = oUser.m_sPreferences;
        this.m_iStatus = oUser.m_iStatus;
        this.m_iID = oUser.m_iID;
        this.oStatics = new CStatistics(oUser.oStatics);
    }
    public CStatistics getStatics()
    {
        return this.oStatics;
    }
    public void AddStars(int iStars)
    {
        this.oStatics.m_iStars += iStars;
    }

    public int getStars()
    {
        return this.oStatics.m_iStars;
    }
    public int getTotalStars() {return this.oStatics.m_iTotalStars;}

    public void setStatus(int iStatus)
    {
        this.m_iStatus = iStatus;
    }
    public int getStatus()
    {
        return this.m_iStatus;
    }
}

