package sw.lalo.taskr.calendar.model;

/**
 * Created by Eduardo on 24/01/2016.
 */


/**
 * Created by Eduardo on 02/11/2015.
 */
public class Ctask {
    public String m_sTitle;
    public String m_sDate;
    public int m_iStartHour;
    public int m_iStartMinute;
    public int m_iEndHour;
    public int m_iEndMinute;
    public int m_iStars;
    public int m_iID;
    private boolean m_boStatus;
    public boolean m_boAlarmSet;


    public String sFileString;

    public Ctask(String sTitle,String sDate,int iStartHour
            ,int iStartMinute,int iEndHour, int iEndMinute,int iStars,boolean boStatus,int iID,boolean boAlarmSet)
    {
        this.m_sTitle = sTitle;
        this.m_sDate = sDate;
        this.m_iStartHour = iStartHour;
        this.m_iStartMinute = iStartMinute;
        this.m_iEndHour = iEndHour;
        this.m_iEndMinute = iEndMinute;
        this.m_iStars= iStars;
        this.m_iID = iID;
        this.m_boStatus = boStatus; /*true = task done*/
        this.m_boAlarmSet = boAlarmSet;
/*
        sFileString = "C#3$/"+this.sTitle+"C#3$/"+this.sDescription+
                "C#3$/"+this.iYear+"C#3$/"+this.iMonth+"C#3$/"+this.iStartHour+"C#3$/"+
                this.iStartMinute+"C#3$/"+this.iEndHour+"C#3$/"+this.iEndMinute+"C#3$/"+
                this.iStars +"C#3$/"+this.iID+"C#3$/"+this.boStatus;*/
    }

    public Ctask()
    {
        this.m_sTitle = "";
        this.m_sDate ="";
        this.m_iStartHour = 0;
        this.m_iStartMinute = 0;
        this.m_iEndHour = 0;
        this.m_iEndMinute = 0;
        this.m_iStars= 1;
        this.m_iID = 0;
        this.m_boStatus = false; /*true = task done*/
        this.m_boAlarmSet = false;
    }
    public void fillFromJASON()
    {

    }
    /*
    public void fillFromString(String sFromFile)
    {

        int indexS = 0;
        int indexF = 0;
        int length = sFromFile.length();
        String staskDescription;
        String StringTemp ="";

        indexS = sFromFile.indexOf("C#3$/", indexS)+5;
        indexF = sFromFile.indexOf("C#3$/", indexS);
        StringTemp =sFromFile.substring(indexS, indexF);
        if (StringTemp != null && !StringTemp.isEmpty() && !StringTemp.equals("null")) {
            this.sTitle =StringTemp;
        }

        indexS = sFromFile.indexOf("C#3$/", indexS)+5;
        indexF = sFromFile.indexOf("C#3$/", indexS);
        StringTemp =sFromFile.substring(indexS, indexF);
        if (StringTemp != null && !StringTemp.isEmpty() && !StringTemp.equals("null")) {
            this.sDescription =StringTemp;
        }

        indexS = sFromFile.indexOf("C#3$/", indexS)+5;
        indexF = sFromFile.indexOf("C#3$/", indexS);
        StringTemp = sFromFile.substring(indexS, indexF);
        if (StringTemp != null && !StringTemp.isEmpty() && !StringTemp.equals("null")) {
            this.iYear = Integer.parseInt(StringTemp);
        }

        indexS = sFromFile.indexOf("C#3$/", indexS)+5;
        indexF = sFromFile.indexOf("C#3$/", indexS);
        StringTemp = sFromFile.substring(indexS, indexF);
        if (StringTemp != null && !StringTemp.isEmpty() && !StringTemp.equals("null")) {
                this.iMonth = Integer.parseInt(StringTemp);
        }

        indexS = sFromFile.indexOf("C#3$/", indexS)+5;
        indexF = sFromFile.indexOf("C#3$/", indexS);
        StringTemp = sFromFile.substring(indexS, indexF);
        if (StringTemp != null && !StringTemp.isEmpty() && !StringTemp.equals("null")) {
            this.iStartHour = Integer.parseInt(StringTemp);
        }

        indexS = sFromFile.indexOf("C#3$/", indexS)+5;
        indexF = sFromFile.indexOf("C#3$/", indexS);
        StringTemp = sFromFile.substring(indexS, indexF);
        if (StringTemp != null && !StringTemp.isEmpty() && !StringTemp.equals("null")) {
            this.iStartMinute = Integer.parseInt(StringTemp);
        }

        indexS = sFromFile.indexOf("C#3$/", indexS)+5;
        indexF = sFromFile.indexOf("C#3$/", indexS);
        StringTemp = sFromFile.substring(indexS, indexF);
        if (StringTemp != null && !StringTemp.isEmpty() && !StringTemp.equals("null")) {
            this.iEndHour = Integer.parseInt(StringTemp);
        }

        indexS = sFromFile.indexOf("C#3$/", indexS)+5;
        indexF = sFromFile.indexOf("C#3$/", indexS);
        StringTemp = sFromFile.substring(indexS, indexF);
        if (StringTemp != null && !StringTemp.isEmpty() && !StringTemp.equals("null")) {
            this.iEndMinute= Integer.parseInt(StringTemp);
        }

        indexS = sFromFile.indexOf("C#3$/", indexS)+5;
        indexF = sFromFile.indexOf("C#3$/", indexS);
        StringTemp = sFromFile.substring(indexS, indexF);
        if (StringTemp != null && !StringTemp.isEmpty() && !StringTemp.equals("null")) {
            this.iStars = Integer.parseInt(StringTemp);
        }
        this.iStars= Integer.valueOf(sFromFile.substring(indexS, indexF));

        indexS = sFromFile.indexOf("C#3$/", indexS)+5;
        indexF = sFromFile.indexOf("C#3$/", indexS);
        StringTemp = sFromFile.substring(indexS, indexF);
        if (StringTemp != null && !StringTemp.isEmpty() && !StringTemp.equals("null")) {
            this.iID = Integer.parseInt(StringTemp);
        }


        indexS = sFromFile.indexOf("C#3$/", indexS)+5;
        StringTemp = sFromFile.substring(indexS);
        if (StringTemp != null && !StringTemp.isEmpty() && !StringTemp.equals("null")) {
            this.boStatus = Boolean.parseBoolean(StringTemp);
        }


    }*/

    public void completeTask()
    {
        this.m_boStatus = true;
    }
    public boolean getStatus()
    {
        return this.m_boStatus;
    }


}
