package sw.lalo.taskr.calendar.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class SQLConstructorTask extends SQLiteOpenHelper {

    public static final String TABLE_TASKS = "TASKS";

    public static final String COLUMN_ID = "_id"; //0
    public static final String COLUMN_USERID = "USER_ID";//1
    public static final String COLUMN_USER = "USER";//2
    public static final String COLUMN_TITLE = "TITLE";//3
    public static final String COLUMN_DATE= "DATE";//4
    public static final String COLUMN_STARTHOUR = "STARTHOUR";//5
    public static final String COLUMN_STARTMINUTE = "STARTMINUTE";//6
    public static final String COLUMN_ENDHOUR = "ENDHOUR";//7
    public static final String COLUMN_ENDMINUTE = "ENDMINUTE";//8
    public static final String COLUMN_STARS = "STARS";//9
    public static final String COLUMN_STATUS = "STATUS";//10
    public static final String COLUMN_ALARM = "ALARM";//11

    private static final String DATABASE_NAME = "Tasks_C.db";
    private static final int DATABASE_VERSION = 118;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_TASKS + "(" + COLUMN_ID
            + " integer primary key autoincrement, " +
            COLUMN_USERID + " text not null, "+
            COLUMN_USER + " text not null, "+
            COLUMN_TITLE + " text not null, "+
            COLUMN_DATE + " text not null, "+
            COLUMN_STARTHOUR + " integer, "+
            COLUMN_STARTMINUTE + " integer, "+
            COLUMN_ENDHOUR + " integer, "+
            COLUMN_ENDMINUTE + " integer, "+
            COLUMN_STARS + " integer, "+
            COLUMN_STATUS + " integer, "+
            COLUMN_ALARM + " integer "+
            ");";

    public SQLConstructorTask(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        database.execSQL(DATABASE_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
      /*  Log.w(SQLConstructorTask.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");*/
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TASKS);
        onCreate(db);
    }

}
