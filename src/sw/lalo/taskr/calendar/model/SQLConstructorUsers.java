package sw.lalo.taskr.calendar.model;

/**
 * Created by Eduardo on 24/01/2016.
 */
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
//import android.util.Log;

public class SQLConstructorUsers extends SQLiteOpenHelper {

    public static final String TABLE_USERS = "USERS";
    public static final String COLUMN_ID = "_id"; //0
    public static final String COLUMN_NAME = "NAME";//1
    public static final String COLUMN_PASSWORD = "PASSWORD";//2
    public static final String COLUMN_STATUS = "STATUS";//3
    public static final String COLUMN_STARS = "STARS";//4
    public static final String COLUMN_TOTALSTARS = "TOTALSTARS";//5
    public static final String COLUMN_PREFERENCES = "PREFERENCES";//6
    public static final String COLUMN_TROPHYSTARS = "TROPHYSTARS";//7
    public static final String COLUMN_TROPHYFIVESTARS = "TROPHYFIVESTARS ";//8
    public static final String COLUMN_TROPHYTASKS= "TROPHYTASKS";//9
    public static final String COLUMN_BONIFICATIONSTARS = "BONIFICATIONSTARS";//10
    public static final String COLUMN_BONIFICATIONTASKS = "BONIFICATIONTASKS";//11




    private static final String DATABASE_NAME = "Users_C.db";
    private static final int DATABASE_VERSION = 234;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_USERS + "(" + COLUMN_ID
            + " integer primary key autoincrement, " +
            COLUMN_NAME + " text not null, "+
            COLUMN_PASSWORD + " text not null, "+
            COLUMN_STATUS+ " integer, "+
            COLUMN_STARS+ " integer, "+
            COLUMN_TOTALSTARS+ " integer, "+
            COLUMN_PREFERENCES + " text not null, "+
            COLUMN_TROPHYSTARS+ " integer, "+
            COLUMN_TROPHYFIVESTARS+ " integer, "+
            COLUMN_TROPHYTASKS+ " integer, "+
            COLUMN_BONIFICATIONSTARS+ " integer, "+
            COLUMN_BONIFICATIONTASKS+ " integer "+
            ");";

    public SQLConstructorUsers(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        database.execSQL(DATABASE_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
       /* Log.w(SQLConstructorUsers.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");*/
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        onCreate(db);
    }

}